<?php
/*
    CUSTOMIZER
*/
function hackerucom_customize ( $wp_customize ){
    if ( ! isset( $wp_customize ) ) {
        return;
    }

        /*
     * University Social
     */
    $wp_customize->add_section('social_links',[
        'title' => esc_html__('Header Social Links','HackeruCom'),
        'description' => esc_html__('Here you can define a social url','HackeruCom'),
    ]);


    foreach (App::$socialIcons as $value) {
        $wp_customize->add_setting( "social_{$value}_url" , array(
            "default"           => "",
            )
        );
        $wp_customize->add_control( "social_{$value}_url_control", array(
            "label"    => esc_html__( "{$value} Address", "HackeruCom" ),
            "section"  => "social_links",
            "settings" => "social_{$value}_url",
            "type"     => "text",
            )
        );
    }

      /**
     * General Information
     */
    $wp_customize->add_section('general_info',[
        'title' => esc_html__('General Information','HackerUcom'),
        'description' => "
        Here you can define Disclaimer",
    ]);

    $wp_customize->add_setting('disclaimer' , array(
        'default' => 'By clicking "Submit" I consent to be contacted by or on behalf of the University of Miami,
        including by email, calls, and text messages to any telephone number that I provide, about my educational interest. I understand my
        consent is not required to purchase or enroll. I also agree to the <a target="_blank" rel="noreferrer noopener" aria-label="This is an external link (opens in a new tab)" href=""> Terms of Use </a> and <a target="_blank" rel="noreferrer noopener" aria-label="This is an external link (opens in a new tab)" href="https://www.hackerusa.com/privacy-policy/"
        rel="noopener"> Privacy Policy.</a>',
        'description' => "",
        )
    );

    $wp_customize->add_control( 'disclaimer_control', array(
        'label'    => esc_html__( 'Disclaimer', 'HackerUcom' ),
        'section'  => 'general_info',
        'settings' => 'disclaimer',
        'type'     => 'textarea',
        )
    );


       /**
     * Footer
     */
    $wp_customize->add_section('footer_general_info',[
        'title' => esc_html__('Footer General Information','HackeruCom'),
        'description' => esc_html__('Here you can define the general information of the university','HackeruCom'),
    ]);

        /**
     * HackeruCom Logo
     */
    $wp_customize->add_setting( 'hackeru_logo' , array(
        'default'           => '',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Upload_Control(
        $wp_customize,
        'hackeru_logo',
        array(
            'label'      => __( 'HackerU  Logo', 'HackeruCom' ),
            'section'    => 'footer_general_info',
            'settings'   => 'hackeru_logo',
        ) )
    );

        /**
     * Footer headline 1
    */
    $wp_customize->add_setting( 'footer_headline_1' , array(
        'default'           => 'Join Our Global Community',
        )
    );

    $wp_customize->add_control( 'footer_headline_1_control', array(
        'label'    => esc_html__( 'footer headline 1', 'HackeruCom' ),
        'section'  => 'footer_general_info',
        'settings' => 'footer_headline_1',
        'type'     => 'textarea',
        )
    );

    /**
     * Footer Para
    */
    $wp_customize->add_setting( 'footer_para' , array(
        'default'           => 'We believe that expertise is gained via experience. As the leading provider of knowledge transfer in Israel, HackerU boasts the highest level of expertise, straight from the heart of the world-renowned Israeli Cyber Ecosystem.',
        )
    );

    $wp_customize->add_control( 'footer_para_control', array(
        'label'    => esc_html__( 'footer para', 'HackeruCom' ),
        'section'  => 'footer_general_info',
        'settings' => 'footer_para',
        'type'     => 'textarea',
        )
    );

    /*
     * Footer Urls
     */
    foreach (App::$footerUrls as $value) {
        $wp_customize->add_setting( "{$value}_url" , array(
            "default"           => "",
            )
        );
        $wp_customize->add_control( "{$value}_control", array(
            "label"    => esc_html__( "{$value} Address", "HackeruCom" ),
            "section"  => "footer_general_info",
            "settings" => "{$value}_url",
            "type"     => "text",
            )
        );
    }

    /*
     * Social Links
     */
    foreach (App::$socialIcons as $value) {
        $wp_customize->add_setting( "social_{$value}_url" , array(
            "default"           => "",
            )
        );
        $wp_customize->add_control( "{$value}_control", array(
            "label"    => esc_html__( "{$value} Address", "HackeruCom" ),
            "section"  => "footer_general_info",
            "settings" => "social_{$value}_url",
            "type"     => "text",
            )
        );
    }

    /*
     * Rights
     */

    $wp_customize->add_setting( 'copyright_text' , array(
        'default'           => '© 2020 HackerU. All Rights Reserved.',
        )
    );

    $wp_customize->add_control( 'copyright_text_control', array(
        'label'    => esc_html__( 'footer rights', 'HackeruCom' ),
        'section'  => 'footer_general_info',
        'settings' => 'copyright_text',
        'type'     => 'textarea',
        )
    );

}



add_action( 'customize_register', 'hackerucom_customize',1 );
