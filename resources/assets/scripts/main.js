// import external dependencies
/*eslint no-undef: "off"*/
/*eslint no-unused-vars: "off"*/

import 'jquery';

// Import everything from autoload
import './autoload/**/*';

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import ourPrograms from './routes/ourPrograms';
import academicInstitutions from './routes/academicInstitutions';
import career from './routes/career';
import hamburger from './util/hamburger';
import carouselPprograms from './util/carouselPprograms';
import toggle from './util/togglePopUp';
import mainAccordion from './util/mainAccordion';
import homeAccordion from './util/homeAccordion';
import numbers from './util/numbers';
import successCarousel from './util/successCarousel';
import ecosystem_accordion from './util/ecosystem-accordion';
import partners from './util/partners';
import ecosystem from './util/ecosystem';
import testimonials from './util/testimonials';
import providerCarousel from './util/providerCarousel';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  // aboutUs,

  academicInstitutions,

  ourPrograms,

  career,

});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

//Menu Hamburger
hamburger();

toggle();

// swiper();
window.onload = carouselPprograms.init();

window.onload = partners.init();

window.onload = ecosystem.init();
window.onload = successCarousel.init();
window.onload = testimonials.init();
window.onload = providerCarousel.init();

mainAccordion();

homeAccordion();

numbers();

// successCarousel();

ecosystem_accordion();
