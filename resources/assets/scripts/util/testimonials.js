import Swiper,{ Scrollbar, Pagination,Autoplay }  from 'swiper';
Swiper.use([Scrollbar, Pagination,Autoplay]);

export default  {
  init() {
    new Swiper('.swiper-testimonials', {
      direction: 'horizontal',
      autoplay: true,
      loop: true,
      // speed: 600,
      slidesPerView: 2.5,
      spaceBetween: 15,
      breakpoints:{
      992:{
        slidesPerView: 2.5,
      },
      768: {
        slidesPerView: 1.3,
      },

      370:{
        slidesPerView: 1.4,
        spaceBetween: 50,
      },
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
      },
    });

    // swiper();
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },
}
