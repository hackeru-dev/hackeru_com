
const mainAcordion = () =>{

  $(document).ready(function() {
    ////Main Accordion
    let acadItem = document.querySelectorAll('.custom-accordion__left__course');
    let acadDescrip = document.querySelectorAll('.custom-accordion__right__course');
    let upBtn = document.getElementById('accord_btn_up');
    let downBtn = document.getElementById('accord_btn_down');

    const currentState = {
      count: 0,
      maxNum: acadItem.length-1,
    };
      ////Arrow trigger:
    downBtn.addEventListener('click', function(){
      $('.custom-accordion__right').find('[class*="active"]').removeClass('custom-accordion__right__course--active');
      $('.custom-accordion__left__course').find('[class*="active"]').removeClass('custom-accordion__left__course--active');

      if(currentState.count != currentState.maxNum) {
        acadItem[currentState.count].classList.remove('custom-accordion__left__course--active');
        acadDescrip[currentState.count].classList.remove('custom-accordion__right__course--active');
        currentState.count++;
        acadItem[currentState.count].classList.add('custom-accordion__left__course--active');
        acadDescrip[currentState.count].classList.add('custom-accordion__right__course--active');
      }
      else{
        currentState.count=0;
        acadItem[currentState.count].classList.add('custom-accordion__left__course--active');
        acadItem[currentState.maxNum].classList.remove('custom-accordion__left__course--active');

        acadDescrip[currentState.count].classList.add('custom-accordion__right__course--active');
        acadDescrip[currentState.maxNum].classList.remove('custom-accordion__right__course--active');
      }
    });

    upBtn.addEventListener('click', function(){
      $('.custom-accordion__right').find('[class*="active"]').removeClass('custom-accordion__right__course--active');
      $('.custom-accordion__left__course').find('[class*="active"]').removeClass('custom-accordion__left__course--active');

      if(currentState.count <= currentState.maxNum) {
          acadItem[currentState.count].classList.remove('custom-accordion__left__course--active');
          acadDescrip[currentState.count].classList.remove('custom-accordion__right__course--active');
          if(parseInt(currentState.count) !== 0) {
            currentState.count--;
          }
          else if(parseInt(currentState.count) === 0){
            currentState.count = currentState.maxNum;
          }
          acadItem[currentState.count].classList.add('custom-accordion__left__course--active');
          acadDescrip[currentState.count].classList.add('custom-accordion__right__course--active');
      } else {
          currentState.count=0;
          acadItem[currentState.count].classList.add('custom-accordion__left__course--active');
          acadItem[currentState.maxNum].classList.remove('custom-accordion__left__course--active');

          acadDescrip[currentState.maxNum].classList.remove('custom-accordion__right__course--active');
          acadDescrip[currentState.count].classList.add('custom-accordion__right__course--active');
      }
    });

////Main Accordion End
    var ignore = document.getElementById('toIgnore');
    document.querySelectorAll('.custom-accordion__left').forEach(div => {
      div.addEventListener('click', function (evnt) {
        var {
          target,
        } = evnt;

        console.log('target.className',target.className);

        target = evnt.target;
        if ( target === ignore || ignore.contains(target)) {
          return;
        } else if (target.className.includes('accordion-parent-node')) {
          return;
        }
        else if (target.className.includes('custom-accordion-p--1')) {
          return;
        }

        const boxToShow = target.dataset.id;
        let position = `[data-position="${div.dataset.position}"]`;
        // reset active class on the right box
        $(position).find(['class*="active"']).removeClass('custom-accordion__right__course--active');

        $(`${position} + .custom-accordion__right > .custom-accordion__right__course--active`).toggleClass(
          'custom-accordion__right__course--active');
        $(`${position} .custom-accordion__left__course--active`).toggleClass(
          'custom-accordion__left__course--active');
        target.classList.toggle('custom-accordion__left__course--active');
        $('#' + boxToShow).toggleClass('custom-accordion__right__course--active');
        $('#' + boxToShow).show();
        if(position == '[data-position="top"]') {
          $([document.documentElement, document.body]).animate({
            scrollTop: $(`#${boxToShow}`).offset().top + (-200),
        }, 'slow');
        }
      });
    });

    $('.accordion__toggle-item').on('click', function () {
      $(this).toggleClass('accordion__toggle-item--active');
      $(this).next().slideToggle(600);
    });
    // Popup
    // toggle();
  // finalize() {
  //   // JavaScript to be fired on the faq page, after the init JS
  // },

})
}

export default mainAcordion;

