const numbers = () =>{

  var reset = function reset() {
    if ($(this).scrollTop() > 350) {
        $(this).off('scroll');

        $('.numbers-number').each(function (i, el) {
          var data = parseInt(this.dataset.n, 10);
          var props = {
              'from': {
                  'count': 0,
              },
                  'to': {
                  'count': data,
              },
          };
          $(props.from).animate(props.to, {
              duration: 1000 * 1,
              step: function (now) {
                  $(el).text(Math.ceil(now));
              },
              complete:function() {
                  if (el.dataset.sym !== undefined) {
                    el.textContent = el.textContent.concat(el.dataset.sym)
                  }
              },
          });
      });
    }
  };

  $(window).on('scroll', reset);
}

export default numbers;



