
import anime from 'animejs/lib/anime.es.js';

const careerNumbers = () =>{
  var flag = true;
  $(document).scroll(function () {
    var y = $(this).scrollTop();
    var x = $('.numbers-section').offset().top-500;

    // console.log(y);
    if (y >= x) {
      if(flag){
        flag = false;
        $('.careerNumbers-number').each(function (i, el) {
          var data = parseInt(this.dataset.n, 10);
          var props = {
              'from': {
                  'count': 0,
              },
                  'to': {
                  'count': data,
              },
          };
          $(props.from).animate(props.to, {
              duration: 1000 * 1,
              step: function (now) {
                  $(el).text(Math.ceil(now));
              },
              complete:function() {
                  if (el.dataset.sym !== undefined) {
                    el.textContent = el.textContent.concat(el.dataset.sym)
                  }
              },
          });
      });

      ///////////////////////////////////////////////////////////

      var el = document.querySelector('.career-text');
      var el2 = document.querySelector('.career-text--2');
      el.innerHTML = el.textContent.replace(/\S/g, '<span class="letter">$&</span>');
      el2.innerHTML = el2.textContent.replace(/\S/g, '<span class="letter">$&</span>');
      console.log(el);

      anime.timeline({loop: false})
        .add({
          targets: '.career-text .letter',
          scale: [4,1],
          opacity: [0,1],
          translateZ: 0,
          easing: 'easeOutExpo',
          duration: 950,
          delay: (el, i) => 70*i,
        })

        anime.timeline({loop: false})
        .add({
          targets: '.career-text--2 .letter',
          scale: [4,1],
          opacity: [0,1],
          translateZ: 0,
          easing: 'easeOutExpo',
          duration: 950,
          delay: (el, i) => 70*i,
        })
      }
    }

  });

}

export default careerNumbers;



