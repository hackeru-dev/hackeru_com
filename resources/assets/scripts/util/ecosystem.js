/*eslint no-undef: "off"*/
/*eslint no-unused-vars: "off"*/

import Swiper,{ Scrollbar, Pagination,Autoplay }  from 'swiper';
Swiper.use([Scrollbar, Pagination,Autoplay]);

export default  {
  init() {
    console.log('ecosystem');

    var swiper = new Swiper('.swiper-ecosystem', {
      direction: 'horizontal',
      autoplay: true,
      loop: true,
      slidesPerView: 3,
      spaceBetween: 15,
      breakpoints:{
        300:{
          spaceBetween: 0,
          slidesPerView: 3,
        },
        768: {
          spaceBetween:15,
          slidesPerView: 3,
        },
        1024: {
          spaceBetween:15,
          slidesPerView: 3,
        },
      },
    });
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },
}
