/*eslint no-undef: "off"*/
/*eslint no-unused-vars: "off"*/

import Swiper,{ Scrollbar, Pagination,Autoplay }  from 'swiper';
Swiper.use([Scrollbar, Pagination,Autoplay]);

export default  {
  init() {
    console.log ('success csrousel');
    var swiper = new Swiper('.swiper-success', {
      direction: 'horizontal',
      centeredSlides: true,
      // autoplay: true,
      loop: true,
      speed: 600,
      slidesPerView: 2.5,
      spaceBetween: 30,
      breakpoints:{
    300:{
        slidesPerView: 1.2,
        spaceBetween: 20,
      },
      768: {
        spaceBetween:15,
        slidesPerView: 1.8,
      },
      1024: {
        spaceBetween:15,
        slidesPerView: 2.5,
      },
      1440: {
        spaceBetween:15,
        slidesPerView: 2.5,
      },
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
      },
    });

    $('[class*="swiper-slide"]').on('click', function(){
      $('.swiper-slide').find('.success-carousel__slide').removeClass('blur');
      $(this).next().find('.success-carousel__slide').addClass('blur');
    })
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },

}

