import Swiper,{ Navigation, Pagination,Autoplay }  from 'swiper';
Swiper.use([Navigation, Pagination,Autoplay]);

export default  {
  init() {

    new Swiper('.swiper-test', {
      centeredSlides: true,
      loop: true,
      slidesPerView: 5,
      spaceBetween: 5,
      autoplay: true,

      breakpoints:{
        300:{
          spaceBetween: 0,
          slidesPerView: 3,
          centeredSlides: true,
        },
        768: {
          spaceBetween:15,
          slidesPerView: 3,
        },
        1024: {
          spaceBetween:15,
          slidesPerView: 3,
        },
        1280: {
          spaceBetween:15,
          slidesPerView: 5,
        },
        1440: {
          spaceBetween:15,
          slidesPerView: 5,
        },
      },
      navigation: {
        nextEl: '.swiper-button-next1',
        prevEl: '.swiper-button-prev1',
      },
    });
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },

}
