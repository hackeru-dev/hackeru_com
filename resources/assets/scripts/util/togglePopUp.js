const toggle = () => {

  $(document).ready(function() {
    $.each($('button[class*=popup-btn-]'), function(i, val){
      $(val).bind('click', function() {ToggleDisplay(`.popup-btn-${i+1}`)});
    });
  })
}

function ToggleDisplay(popupId) {
  let popup =  $(popupId).attr('data-popup');
  display(popup)
}

function display(popup) {
    if ($(`.${popup}`).children().length > 0) {
      $(`.${popup}`).fadeIn(500, function() {
          $(document).bind('click', function(event) {
              hide(event,popup)
          })
      })
  }

}

function hide(event,popup) {
    if (event.target.className.includes('popup') === !1 && event.target.nodeName != 'A') {
      $(`.${popup}`).fadeOut(500, function() {
          $(document).unbind('click')
      })
  }
}


$('.faq__toggle-item').on('click', function () {
  $(this).find('.faq-arrowUp').toggleClass('rotate_img');
})

export default toggle;
