const homeAcordion = () =>{
  var x = window.matchMedia('(max-width: 578px)');
  myFunction(x) ;
  x.addListener(myFunction);
  function myFunction(x) {
    if (x.matches) {
        ///Add active class to right elements in mobile
      $('.academic-accordion__left__course').addClass('academic-accordion__left__course--active');
    }else {
    //   let items = document.querySelectorAll('.academic-accordion__left__course');
    //   let description = document.querySelectorAll('.academic-accordion__right__course');
    //   let btnUp = document.getElementById('academic_btn_up');
    //   let btnDown = document.getElementById('academic_btn_down');

    //   const currentSt = {
    //     count: 0,
    //     maxNum: items.length-1,
    //   };

    //   ////Arrow trigger:
    //   btnDown.addEventListener('click', function(){
    //   if(currentSt.count != currentSt.maxNum) {
    //     $('.academic-accordion__right__course').find('[class*="active"]').removeClass('academic-accordion__right__course--active');
    //     $('.academic-accordion__left__course').find('[class*="active"]').removeClass('academic-accordion__left__course--active');

    //     items[currentSt.count].classList.remove('academic-accordion__left__course--active');
    //     description[currentSt.count].classList.remove('academic-accordion__right__course--active');
    //     currentSt.count++;
    //     items[currentSt.count].classList.add('academic-accordion__left__course--active');
    //     description[currentSt.count].classList.add('academic-accordion__right__course--active');
    //   }
    //   else{
    //     currentSt.count=0;
    //     items[currentSt.count].classList.add('academic-accordion__left__course--active');
    //     items[currentSt.maxNum].classList.remove('academic-accordion__left__course--active');

    //     description[currentSt.count].classList.add('academic-accordion__right__course--active');
    //     description[currentSt.maxNum].classList.remove('academic-accordion__right__course--active');
    //   }
    // });

    // btnUp.addEventListener('click', function(){
    //   $('.academic-accordion__right__course').find('[class*="active"]').removeClass('academic-accordion__right__course--active');
    //   $('.academic-accordion__left__course').find('[class*="active"]').removeClass('academic-accordion__left__course--active');

    //   if(currentSt.count <= currentSt.maxNum) {
    //       items[currentSt.count].classList.remove('academic-accordion__left__course--active');
    //       description[currentSt.count].classList.remove('academic-accordion__right__course--active');
    //       if(parseInt(currentSt.count) !== 0) {
    //         currentSt.count--;
    //       }
    //       else if(parseInt(currentSt.count) === 0){
    //         currentSt.count = currentSt.maxNum;
    //       }
    //       items[currentSt.count].classList.add('academic-accordion__left__course--active');
    //       description[currentSt.count].classList.add('academic-accordion__right__course--active');
    //   } else {
    //       currentSt.count=0;
    //       items[currentSt.count].classList.add('academic-accordion__left__course--active');
    //       items[currentSt.maxNum].classList.remove('academic-accordion__left__course--active');

    //       description[currentSt.count].classList.add('academic-accordion__right__course--active');
    //       description[currentSt.maxNum].classList.remove('academic-accordion__right__course--active');
    //   }
    // });

      var ignore = document.getElementById('toIgnore');
      document.querySelectorAll('.academic-accordion__left').forEach(div => {
        div.addEventListener('click', function (evnt) {
          var {
            target,
          } = evnt;

          target = evnt.target;
          if ( target === ignore || ignore.contains(target)) {
            return;
          } else if (target.className.includes('accordion-parent-node')) {
            return;
          }else if (target.className.includes('academic-icon')) {
            return;
          }else if (target.className.includes('accordion-wrapper-node')){
            return;
          }else if (target.className.includes('academic-white-box__arrow')){
            return;
          }

          const boxToShow = target.dataset.id;
          let position = `[data-position="${div.dataset.position}"]`;

          $(position).find(['class*="active"']).removeClass('academic-accordion__right__course--active');
          // console.log(position)

          $(`${position} + .academic-accordion__right > .academic-accordion__right__course--active`).toggleClass(
            'academic-accordion__right__course--active');
          $(`${position} .academic-accordion__left__course--active`).toggleClass(
            'academic-accordion__left__course--active');
          target.classList.toggle('academic-accordion__left__course--active');
          $('#' + boxToShow).toggleClass('academic-accordion__right__course--active');
          $('#' + boxToShow).show();
          if(position == '[data-position="bottom"]') {
            $([document.documentElement, document.body]).animate({
              scrollTop: $(`#${boxToShow}`).offset().top + (-200),
          }, 'slow');
          }
        });
      });

      document.querySelectorAll('*[class^="accordion__toggle-item"]').forEach( div => {
        div.addEventListener('click', function() {
          jQuery(this).toggleClass('accordion__toggle-item--active');
          jQuery(this).next().slideToggle(600);
        });
      });
    }
  }
  ///Add active class to right elements in mobile End

// finalize() {
//   // JavaScript to be fired on the faq page, after the init JS
// },
}

export default homeAcordion;
