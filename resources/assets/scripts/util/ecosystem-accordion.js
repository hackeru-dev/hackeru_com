
const ecosystem_accordion = () => {

  // JavaScript to be fired on the faq page
  $('accordion__toggle-item--exams').on('click', function () {
    $(this).toggleClass('accordion__toggle-item--exams--active');
    $(this).next().slideToggle(600);
  });

  var local_btn = document.getElementById('local_btn');
  var global_btn = document.getElementById('global_btn');
  // let button_parent_class = 'ecosystem-accordion__left__buttons';
  let button_ui_active_class = '.ecosystem-btn';
  let buttons_active_class = 'ecosystem-btn--active';
  let li_active_class = 'ecosystem-accordion__left__course--active';
  let z = 999;

  /*
    Logic when clicking the buttons
  */

  // adding event listener for buttons
  $('.ecosystem-btn').on ('click', function(evnt){

    let parent = evnt.target.closest('[data-parent]');
    let parent_position = parent.dataset.parent;
    let currentBtn = $(this)

    $('.white-box').find('[class*="--active"]').each(function(i, val){
      let tst = $(val).attr('class').split(/\s+/).filter(cls=>cls.includes('--active'));
      // remove from all elements active class
      $(val).removeClass(tst[0])

      // add active class to the elements that active
      $(currentBtn).addClass('ecosystem-btn--active');
      // active the first title
      $(parent).find('[data-id*="box-0"]').addClass(li_active_class);
      // add active class to the description box

      let description_box_related__to_btn = $('.white-box').find(`.ecosystem-accordion__right[data-parent=${parent_position}]`);
      $(description_box_related__to_btn).addClass('ecosystem-accordion__right--active')
      $(description_box_related__to_btn).find('[id*=box-0]').addClass('ecosystem-accordion__right__course--active');
    });

      // ////Mobile
      var x = window.matchMedia('(max-width: 767px)');

      function myFunction(x) {
        if(x.matches ){
          if($(currentBtn).hasClass('ecosystem-btn--active')){
            $('.mobile').removeClass('mobile');
            $(parent).find('[data-id*="box-"]').addClass('mobile');

            ///Add z-index to the  parent on click
            $(parent).css('z-index', z++ );
          }
        }
      }
      x.addListener(myFunction);
      myFunction(x) ;
  });

  /*
    Ading event listeners to lists
  */
  document.querySelectorAll('.ecosystem-accordion__left').forEach(div => {
    div.addEventListener('click', function (evnt) {
      var {
        target,
      } = evnt;

      target = evnt.target;

      if ( target === local_btn || local_btn.contains(target)) {
        return;
      } else if(target === global_btn || global_btn && global_btn.contains(target)){
        return;
      } else if (target.className.includes('accordion-parent-node')) {
        return;
      }
      // reset buttons active class
      document.querySelectorAll(button_ui_active_class).forEach(btn=>$(btn).removeClass(buttons_active_class));

      const boxToShow = target.dataset.id;
      let position = `[data-position="${div.dataset.position}"]`;

      let parent = evnt.target.closest('[data-parent]');

      // if the curent target is the button then we need to add to him the active class
      if(Array.from(target.classList).includes('ecosystem-btn')){
        target.classList.add(buttons_active_class);
      }


      // let descriptionBox = $(`.ecosystem-accordion__right[data-parent=${parent.dataset.parent}]`);
      let parent_position = parent.dataset.parent;

      $('.white-box').find('[class*="--active"]').each(function(i, val){
        let tst = $(val).attr('class').split(/\s+/).filter(cls=>cls.includes('--active'));
        // remove from all elements active class
        $(val).removeClass(tst[0])
        let description_box_related__to_btn = $('.white-box').find(`.ecosystem-accordion__right[data-parent=${parent_position}]`);
        $(description_box_related__to_btn).addClass('ecosystem-accordion__right--active')
      });
      // the current target is the list then we need to add to the closest button the active class
      $(parent).find(button_ui_active_class).addClass(buttons_active_class)

      $(`${position} + .ecosystem-accordion__right > .ecosystem-accordion__right__course--active`).hide().toggleClass(
        'ecosystem-accordion__right__course--active');
      $(`${position} .ecosystem-accordion__left__course--active`).toggleClass(
        'ecosystem-accordion__left__course--active');
      target.classList.toggle('ecosystem-accordion__left__course--active');
      $('#' + boxToShow).toggleClass('ecosystem-accordion__right__course--active');

        // ////Mobile
        var x = window.matchMedia('(max-width: 767px)');

        function myFunction(x) {
          if(x.matches ){
            if($('.ecosystem-btn').hasClass('ecosystem-btn--active')){
              $(parent).find('[data-id*="box-"]').addClass('mobile');
            }
            }
          }
        x.addListener(myFunction);
        myFunction(x) ;
    });
  });
}

export default ecosystem_accordion;
