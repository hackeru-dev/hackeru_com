import toggle from '../util/togglePopUp';

const specialistAccordion = () => {
  let spItem = document.querySelectorAll('.specialist-program-accordion__left__course');
  let spDescrip = document.querySelectorAll('.specialist-program-accordion__right__course');
  let upperBtn = document.getElementById('specialist-program_btn_up');
  let downBtn = document.getElementById('specialist-program_btn_down');
  const currState = {
    count: 0,
    maxNum: spItem.length-1,
  };

  downBtn.addEventListener('click', function(){
    if(currState.count != currState.maxNum) {
      spItem[currState.count].classList.remove('specialist-program-accordion__left__course--active');
      spDescrip[currState.count].classList.remove('specialist-program-accordion__right__course--active');
      //console.log(spDescrip[currState.count]);
      currState.count++;
      //console.log(currState.count);
      spItem[currState.count].classList.add('specialist-program-accordion__left__course--active');
      spDescrip[currState.count].classList.add('specialist-program-accordion__right__course--active');
    }
    else{
      currState.count=0;
      spItem[currState.count].classList.add('specialist-program-accordion__left__course--active');
      spItem[currState.maxNum].classList.remove('specialist-program-accordion__left__course--active');

      spDescrip[currState.count].classList.add('specialist-program-accordion__right__course--active');
      spDescrip[currState.maxNum].classList.remove('specialist-program-accordion__right__course--active');
    }
});

  upperBtn.addEventListener('click', function(){
    if(currState.count <= currState.maxNum) {
        spItem[currState.count].classList.remove('specialist-program-accordion__left__course--active');
        spDescrip[currState.count].classList.remove('specialist-program-accordion__right__course--active');
        if(parseInt(currState.count) !== 0) {
          currState.count--;
        }
        else if(parseInt(currState.count) === 0){
          currState.count = currState.maxNum;
        }
        spItem[currState.count].classList.add('specialist-program-accordion__left__course--active');
        spDescrip[currState.count].classList.add('specialist-program-accordion__right__course--active');
    } else {
        currState.count=0;
        spItem[currState.count].classList.add('specialist-program-accordion__left__course--active');
        spItem[currState.maxNum].classList.remove('specialist-program-accordion__left__course--active');

        spDescrip[currState.count].classList.add('specialist-program-accordion__right__course--active');
        spDescrip[currState.maxNum].classList.remove('specialist-program-accordion__right__course--active');
    }
  })

var ignore = document.getElementById('toIgnore');

$('.accordion__toggle-item--exams').on('click', function () {
  $(this).toggleClass('accordion__toggle-item--exams--active');
  $(this).next().slideToggle(600);
});

document.querySelectorAll('.specialist-program-accordion__left').forEach(div => {
  div.addEventListener('click', function (evnt) {
    var {
      target,
    } = evnt;

    // console.log('target.className',target.className);

    target = evnt.target;
    if ( target === ignore || ignore.contains(target)) {
      return;
    } else if (target.className.includes('specialist-accordion-parent-node')) {
      return;
    } else if (target.className.includes('wrapper-node')) {
      return;
    } else if (target.className.includes('arrow-node')) {
      return;
    }else if (target.id.includes('specialist-program_btn')) {
      return;
    }


    const boxToShow = target.dataset.id;
    let position = `[data-position="${div.dataset.position}"]`;
    // console.log(position)
    $(`${position} + .specialist-program-accordion__right > .specialist-program-accordion__right__course--active`).hide().toggleClass(
      'specialist-program-accordion__right__course--active');
    $(`${position} .specialist-program-accordion__left__course--active`).toggleClass(
      'specialist-program-accordion__left__course--active');
    // console.log('previous active:',document.querySelector(`${position} + .specialist-program-accordion__right > .specialist-program-accordion__right__course--active`));
    // console.log(document.querySelector(`${position}`));
    target.classList.toggle('specialist-program-accordion__left__course--active');
    $('#' + boxToShow).toggleClass('specialist-program-accordion__right__course--active');
    $('#' + boxToShow).show();
    // var x = $(`#${boxToShow}`).parent();
    // console.log(x);
    // console.log((position == '[data-position="top"]'));
    // if(position == '[data-position="top"]') {
    //   $([document.documentElement, document.body]).animate({
    //     scrollTop: $(`#${boxToShow}`).offset().top + (-200),
    // }, 'slow');
    // }
  });
});


document.querySelectorAll('*[class^="accordion__toggle-item"]').forEach( div => {
  div.addEventListener('click', function() {
    jQuery(this).toggleClass('accordion__toggle-item--active');
    jQuery(this).next().slideToggle(600);
  });
});

  // Popup
  toggle();
}

export default specialistAccordion;
