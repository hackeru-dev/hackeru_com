
/*eslint no-undef: "off"*/
/*eslint no-unused-vars: "off"*/

import Swiper,{ Scrollbar, Pagination,Autoplay }  from 'swiper';
Swiper.use([Scrollbar, Pagination,Autoplay]);

export default  {
  init() {
    console.log('programs');

    var swiper = new Swiper('.swiper-programs', {
      direction: 'horizontal',
      autoplay: true,
      loop: true,
      // speed: 600,
      slidesPerView: 4,
      spaceBetween: 15,
      breakpoints:{
      992:{
        slidesPerView: 4,
      },
      768: {
        slidesPerView: 2,
      },

      370:{
        slidesPerView: 2,
      },
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
      },
    });
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },
}
