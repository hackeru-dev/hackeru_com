
/*eslint no-undef: "off"*/
/*eslint no-unused-vars: "off"*/

import Swiper,{ Scrollbar, Pagination,Autoplay }  from 'swiper';
Swiper.use([Scrollbar, Pagination,Autoplay]);

export default  {
  init() {
    var swiper = new Swiper('.swiper-programs', {
      direction: 'horizontal',
      autoplay: true,
      loop: true,
      // speed: 600,
      slidesPerView: 4,
      spaceBetween: 15,
      breakpoints:{
          // Phone
        300: {
          slidesPerView: 4,
          spaceBetween: 30,
          direction: 'vertical',
        },

        // Ipad
        768: {
          slidesPerView: 2,
          spaceBetween: 2,
        },

        // Ipad
        1024: {
          slidesPerView: 3,
          spaceBetween: 15,
        },
        // xL
        1280: {
          slidesPerView: 4,
          spaceBetween: 30,
        },
        // Laptops
        1440: {
          slidesPerView: 4,
          spaceBetween: 30,
        },
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
      },
    });
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },

}
