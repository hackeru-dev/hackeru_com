export default  {
  init() {
    // JavaScript to be fired on the faq page
    $('.faq__toggle-item').on('click', function () {
      $(this).toggleClass('faq__toggle-item--active');
      $(this).next().slideToggle(600);
    });
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },

}
