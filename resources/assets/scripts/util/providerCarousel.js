
/*eslint no-undef: "off"*/
/*eslint no-unused-vars: "off"*/

import Swiper,{ Scrollbar, Pagination,Autoplay }  from 'swiper';
Swiper.use([Scrollbar, Pagination,Autoplay]);

export default  {
  init() {
    console.log ('provider carousel');

    var swiper = new Swiper('.swiper-provider', {
      direction:'vertical',
      autoplay: true,
      speed: 600,
      slidesPerView: 4,
      spaceBetween: 50,
      loop: true,

      breakpoints:{
      992:{
        slidesPerView: 3,
        spaceBetween: 15,
      },
      768: {
        slidesPerView: 3,
      },

      370:{
        slidesPerView: 3,
        spaceBetween: 15,
      },
      },
    });
  },
  finalize() {
    // JavaScript to be fired on the faq page, after the init JS
  },

}
