export default {
  init() {
/////Home Accordion
   let items = document.querySelectorAll('.academic-accordion__left__course');
      let description = document.querySelectorAll('.academic-accordion__right__course');
      let btnUp = document.getElementById('academic_btn_up');
      let btnDown = document.getElementById('academic_btn_down');

      const currentSt = {
        count: 0,
        maxNum: items.length-1,
      };

      ////Arrow trigger:
      btnDown.addEventListener('click', function(){
      if(currentSt.count != currentSt.maxNum) {
        items[currentSt.count].classList.remove('academic-accordion__left__course--active');
        description[currentSt.count].classList.remove('academic-accordion__right__course--active');
        currentSt.count++;
        items[currentSt.count].classList.add('academic-accordion__left__course--active');
        description[currentSt.count].classList.add('academic-accordion__right__course--active');
      }
      else{
        currentSt.count=0;
        items[currentSt.count].classList.add('academic-accordion__left__course--active');
        items[currentSt.maxNum].classList.remove('academic-accordion__left__course--active');

        description[currentSt.count].classList.add('academic-accordion__right__course--active');
        description[currentSt.maxNum].classList.remove('academic-accordion__right__course--active');
      }
    });

    btnUp.addEventListener('click', function(){
      $('.academic-accordion__right__course').find('[class*="active"]').removeClass('academic-accordion__right__course--active');
      $('.academic-accordion__left__course').find('[class*="active"]').removeClass('academic-accordion__left__course--active');

      if(currentSt.count <= currentSt.maxNum) {
          items[currentSt.count].classList.remove('academic-accordion__left__course--active');
          description[currentSt.count].classList.remove('academic-accordion__right__course--active');
          if(parseInt(currentSt.count) !== 0) {
            currentSt.count--;
          }
          else if(parseInt(currentSt.count) === 0){
            currentSt.count = currentSt.maxNum;
          }
          items[currentSt.count].classList.add('academic-accordion__left__course--active');
          description[currentSt.count].classList.add('academic-accordion__right__course--active');
      } else {
          currentSt.count=0;
          items[currentSt.count].classList.add('academic-accordion__left__course--active');
          items[currentSt.maxNum].classList.remove('academic-accordion__left__course--active');

          description[currentSt.count].classList.add('academic-accordion__right__course--active');
          description[currentSt.maxNum].classList.remove('academic-accordion__right__course--active');
      }
    });
   /////Home Accordion
  },

  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
