import toggle from '../util/togglePopUp';
import specialistAccordion from '../util/specialistAccordion';
import careerNumbers from '../util/careerNumbers';
import faq from '../util/faq';


export default {
  init() {
    /////Home Accordion
  let items = document.querySelectorAll('.career-accordion__left__course');
  let description = document.querySelectorAll('.career-accordion__right__course');
  let btnUp = document.getElementById('career_btn_up');
  let btnDown = document.getElementById('career_btn_down');

  const currentSt = {
    count: 0,
    maxNum: items.length-1,
  };
  console.log(items);

    ////Arrow trigger:

    btnDown.addEventListener('click', function(){
    if(currentSt.count != currentSt.maxNum) {
      items[currentSt.count].classList.remove('career-accordion__left__course--active');
      description[currentSt.count].classList.remove('career-accordion__right__course--active');
      currentSt.count++;
      items[currentSt.count].classList.add('career-accordion__left__course--active');
      description[currentSt.count].classList.add('career-accordion__right__course--active');
    }
    else{
      currentSt.count=0;
      items[currentSt.count].classList.add('career-accordion__left__course--active');
      items[currentSt.maxNum].classList.remove('career-accordion__left__course--active');

      description[currentSt.count].classList.add('career-accordion__right__course--active');
      description[currentSt.maxNum].classList.remove('career-accordion__right__course--active');
    }
  });

  btnUp.addEventListener('click', function(){
    if(currentSt.count <= currentSt.maxNum) {
      items[currentSt.count].classList.remove('career-accordion__left__course--active');
        description[currentSt.count].classList.remove('career-accordion__right__course--active');
        if(parseInt(currentSt.count) !== 0) {
          currentSt.count--;
        }
        else if(parseInt(currentSt.count) === 0){
          currentSt.count = currentSt.maxNum;
        }
        items[currentSt.count].classList.add('career-accordion__left__course--active');
        description[currentSt.count].classList.add('career-accordion__right__course--active');
    } else {
        currentSt.count=0;
        items[currentSt.count].classList.add('career-accordion__left__course--active');
        items[currentSt.maxNum].classList.remove('career-accordion__left__course--active');

        description[currentSt.count].classList.add('career-accordion__right__course--active');
        description[currentSt.maxNum].classList.remove('career-accordion__right__course--active');
    }
  });

    var ignore = document.getElementById('toIgnore');

    // $('career-accordion__toggle-item--exams').on('click', function () {
    //   $(this).toggleClass('career-accordion__toggle-item--exams--active');
    //   $(this).next().slideToggle(600);
    // });

    document.querySelectorAll('.career-accordion__left').forEach(div => {
      div.addEventListener('click', function (evnt) {
        var {
          target,
        } = evnt;

        target = evnt.target;
        if ( target === ignore || ignore.contains(target)) {
          return;
        } else if (target.className.includes('career-accordion-parent-node')) {
          return;
        }else if (target.className.includes('academic-icon')) {
          return;
        }


        const boxToShow = target.dataset.id;
        let position = `[data-position="${div.dataset.position}"]`;
        console.log(position)
        $(`${position} + .career-accordion__right > .career-accordion__right__course--active`).hide().toggleClass(
          'career-accordion__right__course--active');
        $(`${position} .career-accordion__left__course--active`).toggleClass(
          'career-accordion__left__course--active');
        target.classList.toggle('career-accordion__left__course--active');
        $('#' + boxToShow).toggleClass('career-accordion__right__course--active');
        $('#' + boxToShow).show();
        if(position == '[data-position="bottom"]') {
          $([document.documentElement, document.body]).animate({
            scrollTop: $(`#${boxToShow}`).offset().top + (-200),
        }, 'slow');
        }
      });
    });
    document.querySelectorAll('*[class^="accordion__toggle-item"]').forEach( div => {
      div.addEventListener('click', function() {
        jQuery(this).toggleClass('career-accordion__toggle-item--active');
        jQuery(this).next().slideToggle(600);
      });
    });

    ////Accordion Mobile
    $('.career-accordion__toggle-item').on('click', function () {
      $(this).toggleClass('career-accordion__toggle-item--active');
      $(this).next().slideToggle(600);
    });
     ////Accordion Mobile End
    /////Home Accordion END

    // Popup
    toggle();

    specialistAccordion();

    careerNumbers();
    faq.init();
  },

  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
