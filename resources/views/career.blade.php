{{--
  Template Name: Career
--}}

@extends('layouts.app')

@section('content')

<?php
  $heroCareer = get_field('hero');
?>
<section class="hero-career">
  <div class="hero-career__container container">
      <div class="hero-career__headlines">
          <h1 class="hero-career-h--5">{!! $heroCareer['headline_1'] !!}</h1>
          <div class="line"></div>
          <h3 class="hero-career-ah--3">{!! $heroCareer['headline_2'] !!}</h3>
          <p class="hero-career-p--3">{!! $heroCareer['paragraph'] !!}</p>
      </div>
  </div>
</section>

<!--
#=================ACCORDION===================#
-->
<?php
  $careerAccordion = get_field('careerAccordion');
?>
<section class="career-accordion-section {{$specialClass}}">
  <div class="cross cross__dark-blue"></div>
  <img class="career-accordion-section__triangles-left" src="{!! $careerAccordion['image_mobile'] !!}" alt="triangles">

  <div class="career-accordion-section__container container">
    <div class="career-accordion-section__row">

      <article class="career-accordion-section__article">
          <h2 class="career-accordion-section-ah--1">{!! $careerAccordion['headline'] !!}</h2>
          <div class="line"></div>
          <p class="career-accordion-section-p--1" id="toIgnore"> {!! $careerAccordion['paragraph'] !!} </p>
      </article>

            <!--
      #=================MOBILE ACCORDION===================#
      -->
      <section class="career-accordion__mobile">
        <img class="career-white-box__bar_icon_right" src="{!! $careerAccordion['bar_image_mobile'] !!}" alt="laptop">
        @foreach(['inna', 'or', 'arthur'] as $key => $value)
          <div class="mobile-wrapper">
            <h5 class="career-accordion__toggle-item"> On The Global Level</h5>
            <div class="career-accordion__content">
              <p class="mobile-para">Diminish the global workforce deficit by training the next generation of dugutal and cyber experts
                  to build sustainable technology ecosystem around the globe
              </p>
            </div>
          </div>
        @endforeach
      </section>
      <!--
      #=================END MOBILE ACCORDION===================#
      -->

      <div class="career-accordion-wrapper">
          <div class="career-arrow-images">
              <img class="career-arrow-images__icon icon--up" id="career_btn_up" src="{!! $careerAccordion['icon_up'] !!}')" alt="arrow up">
              <img class="career-arrow-images__icon icon--down" id="career_btn_down" src="{!! $careerAccordion['icon_down'] !!}')" alt="arrow down">
          </div>


      <div class="career-white-box">
          <img class="career-white-box__bar_icon_right" src="{!! $careerAccordion['bar_image'] !!}" alt="laptop">

          <div class="career-accordion__left career-accordion-parent-node" data-position="bottom">
                @foreach( $careerAccordion['boxes'] as $key => $box)
                  <h5 data-id="box-{{$key}}-bottom"
                    class="career-accordion__left__course <?= (int)$key===0 ? 'career-accordion__left__course--active' : ''; ?> career-accordion__left__course--1>">
                      {!! $box['box_left']['title'] !!}
                  </h5>
                @endforeach
          </div><!--/custom-accordion__left-->

        <!-- RIGHT -->
        <div class="career-accordion__right career-accordion__right--large">
              @foreach($careerAccordion['boxes'] as $key => $box)
                <div id="box-{{$key}}-{{$position ?? 'bottom'}}"
                  class="career-accordion__right__course <?= $key===0 ? 'career-accordion__right__course--active' : ''; ?> career-accordion__right__course--1">
                    <article class="career-course-article">
                      <h6 class="career-course-title">{!! $box['box_right']['title'] !!}</h6>
                      <div class="line"></div>
                      <p class="career-course-para"> {!! $box['box_right']['paragraph'] !!} </p>
                    </article>
                </div>
              @endforeach
            <!-- END BOXES -->
          </div><!-- /.custom-accordion__right-->
        </div><!-- /.white box-->
      </div>
    </div><!-- /.career-accordion-section__row-->
  </div><!-- /.container-->
</section><!-- /.ACCORDION   -->


{{-- New Jobs Section --}}
@component('components.newJobs',
['newJobs' => get_field('newJobs'),
'specialClass' => 'career',
])@endcomponent



{{-- REQUIRE 2--}}
<?php
  $require = get_field('require');
?>

@component('components.require',
['require' => $require,
'specialClass' => 'career',
])@endcomponent

<!--
#=================SPECIALIST ACCORDION===================#
-->
<?php
  $specialist = get_field('specialist');
?>
<section class="specialist-program">
  <div class="cross"></div>
  <img class="specialist-program__bar_icon_right" src="{!! $specialist['bar_image'] !!}" alt="laptop">
  <div class="specialist-program__container container">
        <article class="specialist-program__headlines">
          <h2 class="specialist-program-ah--1">{!! $specialist['headline'] !!}</h2>
          <div class="line"></div>
          <p class="specialist-program-p--1"> {!! $specialist['paragraph'] !!} </p>
      </article>
    <div class="specialist-program__row">
      <div class="specialist-program-white-box">
      <div class="specialist-program-accordion__left specialist-accordion-parent-node" data-position="top">
          <div class="specialist-program-accordion__left__wrapper wrapper-node">
            <div class="specialist-program-white-box__arrow arrow-node">
                <img class="specialist-program-icon icon--up" id="specialist-program_btn_up" src="{!! $specialist['icon_up'] !!}" alt="arrow up">
                <img class="specialist-program-icon icon--down" id="specialist-program_btn_down" src="{!! $specialist['icon_down'] !!}" alt="arrow down">
            </div>

              <div class="specialist-program-white-box__titles wrapper-node">
                  @foreach($specialist['boxes'] as $key => $box)
                    <h5 data-id="box-{{$key}}-top"
                      class="specialist-program-accordion__left__course <?= (int)$key===0 ? 'specialist-program-accordion__left__course--active' : ''; ?> specialist-program-accordion__left__course--1">
                      {!! $box['box_left']['title']!!}
                    </h5>
                  @endforeach
            </div>
          </div>
        </div><!--/specialist-program-accordion__left-->

        <!-- RIGHT -->
        <div class="specialist-program-accordion__right specialist-program-accordion__right--large">
            @foreach($specialist['boxes'] as $key => $box)
              <div id="box-{{$key}}-{{$position ?? 'top'}}"
                class="specialist-program-accordion__right__course <?= $key===0 ? 'specialist-program-accordion__right__course--active' : ''; ?> specialist-program-accordion__right__course--1">
                    <h4 class="specialist-program-accordion__right__course-ah--4"> {!! $box['box_right']['title']!!}</h4>
                    <div class="line"></div>
                    <p class="specialist-program-accordion__right__course-p--1"> {!! $box['box_right']['paragraph']!!} </p>
              </div>
            @endforeach

          <!-- END BOXES -->
        </div><!-- /.specialist-program-accordion__right-->
      </div>
    </div>
  </div>
</section><!-- /.ACCORDION   -->


{{-- Number Section --}}

<section class="numbers-section">
  <div class="numbers-section__container container">
      <div class="numbers-wrapper">

        {{-- <div class="numbers-wrapper__box" >
          <h3 class="box-ah--2 career-{{$SpecialClass ?? ''}}"> <span class="careerNumbers-{{$headlineSpecialClass ?? ''}}" data-n="600">0</span></h3>
          <p class="box-p--2">In-Class Hours</p>
        </div> --}}
{{--  --}}
        <div class="numbers-wrapper__box" >
          <h3 class="box-ah--2 career-number"> <span class="careerNumbers-number" data-n="600">0</span></h3>
          <p class="box-p--2">In-Class Hours</p>
        </div>

        <div class="numbers-wrapper__box" >
          <h3 class="box-ah--4 career-text">Experiential</h3>
          <p class="box-p--2">Learning</p>
        </div>

        <div class="numbers-wrapper__box" >
          <h3 class="box-ah--2 career-number"><span class="careerNumbers-number" data-n="13">0</span></h3>
          <p class="box-p--2">Specialized <br> Courses</p>
        </div>

        <div class="numbers-wrapper__box" >
          <h3 class="box-ah--4 career-text--2">Professional</h3>
          <p class="box-p--2">Networking</p>
        </div>
    </div>
  </div>
</section>


{{-- Story --}}
@component('components.story',
 ['story' => $story,
 'specialClass' => 'aboutStory',
 ])@endcomponent

{{-- Market --}}

<section class="market">
  <div class="market__container container">
  <img class="market__iconUp" src="@asset('images/home/footer_corners.svg')" alt="icon">
  <h3 class="market--ah--6">Thousands of industry experts took their first step at HackerU their success is testament to our commitment in
  <span class="market--marker">providing the best programs on the market. </span></div>
</section>

{{-- Togle --}}
<section class="faq">
  <div class="faq__container container">
    <h2 class="require-ah--1">Common <br> Question</h2>
        <div class="line"></div>

    <section class="faq__main">
      <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
            <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
      </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification?<br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification?  <br>Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

        <div class="faq__toggle-item faq">
          <h5 class="faq-p--2">
            Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
          </h5>
          <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>


        <div class="faq__toggle-item faq">
            <h5 class="faq-p--2">
              Why require the OSCP/OSWE certification and not the CEH certification? <br> Is one better than the other?
            </h5>
            <img class="faq-arrowUp" src="@asset('images/icons/arrow_down.svg')" alt="icon">
        </div>
        <div class="faq__content">
          <p class="faq__content-p--1">
            The OSCP certification is the most popular yet prestigious offensive security certification. The OSWE certificate is a newly released
            certification that focuses only on web penetration. In the last section of the program, HackerU trainers will work hand-in-hand with our
            learners to ensure their decision and readiness for either the OSCP or OSWE exams.
          </p>
        </div>

    </section>
  </div><!-- /.accordion__container -->
</section><!-- /.accordion -->

<!--
  #================= FOOTER FORM ===================#
-->
@component('components.footer-form',
['footer_form' => $footer_form,
'specialClass' => 'FAQ',
])@endcomponent

@endsection
