<header class="header">
    <nav class="header__nav container">
    {{mytheme_custom_logo()}}
      @php
        wp_nav_menu( array('menu' => 'Header Menu','container'=> 'ul','menu_class'=>'header__nav-menu','items_wrap'=>'<ul class="header__nav-menu">%3$s</ul>'));
      @endphp

      <ul class="header__nav-social">
      @foreach(App::$socialIcons as $key => $value)
      @php
        $currentSocial = get_theme_mod("social_{$value}_url");
      @endphp
        <li class="social_item">
          <a class="social-icon" href="{{$currentSocial}}" target="_blank" rel="noreferrer noopener" aria-label="This is an external link (opens in a new tab)">
            <svg class="icon icon-chevron-left"><use xlink:href="@asset('images/general-icons/social_icons.svg')#{{$value}}"></use></svg>
          </a>
        </li>
      @endforeach
      </ul>
    </nav>

<!-- https://codepen.io/g13nn/pen/eHGEF-->
<!-- https://bhoover.com/simple-jquery-mobile-menu/ -->

  <nav id="mobile">
    <div id="nav-icon3" class="navicon mtoggle">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
    @php
      wp_nav_menu( array('menu' => 'Header Menu','container'=> '','items_wrap'=>'<ul id="mmenu" class="header__nav-menu">%3$s</ul>'));
    @endphp
  </nav>
</header>
