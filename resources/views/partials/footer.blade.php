
@php
  $footer = get_field('footer')
@endphp
<footer class="footer">
  <div class="footer__wrapper">
  <img class="footer__icons footer__icons--corners_icon_right" src="@asset('images/home/bottom_footer_corners.svg')" alt="laptop">
    <picture>
        <source media="(max-width:578px)" srcset="@asset('images/home/collage_corner.svg')">
        <img class="footer__icons footer__icons--bar_icon_left" src="@asset('images/home/footer_icon_left.svg')" alt="laptop">
    </picture>
    {{-- <img class="footer__icons footer__icons--bar_icon_left" src="@asset('images/home/footer_icon_left.svg')" alt="laptop"> --}}

    <img src="{{ get_theme_mod('hackeru_logo') }}" class="footer__logo" alt="Hackeru Logo">

    <h3 class="footer-p--5">
      @if( isset($footer['headline_1']) && strlen($footer['headline_1']) > 0)
          {!! $footer['headline_1'] !!}
      @else
        {!! get_theme_mod('footer_headline_1') !!}
      @endif
    </h3>
    <div class="line"></div>
    <p class="footer-p--1">
          @if( isset($footer['paragraph']) && strlen($footer['paragraph']) > 0)
          {!! $footer['paragraph'] !!}
      @else
          {!! get_theme_mod('footer_para') !!}
    @endif
    </p>

  {{--Footer Urls--}}
    <div class="footer__urls">
      @foreach(App::$footerUrls as $key => $value)
        @php
          $currentUrl = get_theme_mod("{$value}_url");
        @endphp
          @if($currentUrl)
            <a class="urls" href="{{$currentUrl}}" target="_blank" rel="noreferrer noopener" aria-label="This is an external link (opens in a new tab)">
          {{$currentUrl}}
            </a>
          @endif

      @endforeach
      </div>
    {{-- Social Links--}}
    <div class="footer__social-icons">
        @foreach(App::$socialIcons as $key => $value)
            @php
              $currentSocial = get_theme_mod("social_{$value}_url");
            @endphp
          @if( $currentSocial)
              <a class="social-icon" href="{{$currentSocial}}" target="_blank" rel="noreferrer noopener" aria-label="This is an external link (opens in a new tab)">
                <svg class="icon icon-chevron-left"><use xlink:href="@asset('images/general-icons/social_icons.svg')#{{$value}}"></use></svg>
              </a>
          @endif
        @endforeach
    </div>

    <p class="footer__copyright-text">
      @if( isset($footer['copyright_text']) && strlen($footer['copyright_text']) > 0)
          {!! $footer['copyright_text'] !!}
      @else
        {!! get_theme_mod('copyright_text') !!}
      @endif</p>

  </div>
</footer>
<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
