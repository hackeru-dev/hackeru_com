{{--
  Template Name: Academic Institutions
--}}

@extends('layouts.app')

@section('content')

<?php
  $hero_inst = get_field('hero');
?>
<section class="hero-academic-institutions">
  <div class="hero-academic-institutions__container container">
      <div class="hero-academic-institutions__headlines">
          <h2 class="hero-academic-institutions-h--1"> {!! $hero_inst['headline_1'] !!}</h2>
          <h1 class="hero-academic-institutions-h--2">{!! $hero_inst['headline_2'] !!}</h1>
          <div class="line"></div>
          <h2 class="hero-academic-institutions-h--1">{!! $hero_inst['headline_3'] !!}</h2>
          <p class="hero-academic-institutions-p--1">{!! $hero_inst['paragraph'] !!}</p>
      </div>
  </div>
</section>


{{-- NUMBERS SECTION --}}
<?php
  $gap = get_field('gap');
  $numbers = $gap['numbers']
?>
<section class="gap">
    <div class="gap__container container">
        <h2 class="gap-ah--1">{!! $gap['headline'] !!}</h2>
        <div class="line"></div>
        <picture>
          <source media="(max-width:578px)" srcset="{!! $gap['triangles_img_mob'] !!}">
          <img class="gap__icon-corner" src="{!! $gap['triangles_img'] !!}" alt="laptop">
        </picture>
        <img class="gap__icon-bar-mobile" src="{!! $gap['bar_img_mob'] !!}" alt="corners">

        @component('components.numbers',
        ['numbers' => $numbers,
        'specialClass' => 'academicInst',
        ])@endcomponent

    </div>
</section>

{{-- REQUIRE 1--}}
<?php
  $require_1 = get_field('require_1');
?>


@component('components.require',
['require' => $require_1,
'specialClass' => 'institution-top',
])@endcomponent

{{-- REQUIRE 2--}}
<?php
  $require = get_field('require');
?>

@component('components.require',
['require' => $require,
'specialClass' => 'institution-bottom',
])@endcomponent


<!--
#=================ACCORDION===================#
-->
<?php
  $ecosystem_accordion = get_field('ecosystem_accordion');
  $sections = $ecosystem_accordion['sections'];
?>
<section class="accordion-ecosystem">
  <div class="accordion-ecosystem__container container">
    <div class="accordion-ecosystem__row">
      <article class="accordion-ecosystem__article">
        <h2 class="accordion-ecosystem-ah--1">{!! $ecosystem_accordion['headline'] !!}</h2>
        <div class="line"></div>
      </article>

    <div class="white-box">
      <img class="ecosystem-bar-icon-right" src="{!! $ecosystem_accordion['image'] !!}" alt="bar">
        @php
          // here we're using values because ours keys now letters ['local', 'global']
          $upper_current_key=0;
        @endphp
        @foreach($sections as $parent_key => $parent_value)
          <div class="ecosystem-accordion__left accordion-parent-node" data-position="{{$parent_key}}" id="{{$parent_key}}" data-parent="{{$parent_key}}">
              <div class="ecosystem-accordion__left__buttons">
                  <button class="ecosystem-btn btn--2 {{$upper_current_key===0 ? 'ecosystem-btn--active' : ''}}" id="{{$parent_key}}_btn" data-popup="accordion_right--1">From {{$parent_key}}</button>
              </div>
            <div class="ecosystem-accordion__left__box-wrapper {{$parent_key}}">
                @foreach($sections[$parent_key]['boxes'] as $key =>$box )
                  <h5 data-id="box-{{$key}}-{{$parent_key}}"
                    class="ecosystem-accordion__left__course <?= ($key===0 && $upper_current_key===0) ? 'ecosystem-accordion__left__course--active mobile' : ''; ?> ecosystem-accordion__left__course--1>">
                    {!! $box['box_left']['title'] !!}
                  </h5>
                @endforeach
            </div>
          </div><!--/custom-accordion__left-->
          @php
            $upper_current_key++;
          @endphp
        @endforeach

        <!-- Description Box -->
        @php
          // here we're using values because ours keys now letters ['local', 'global']
          $lower_current_key=0;
        @endphp
        @foreach($sections as $position_key => $position)
          <div class="ecosystem-accordion__right ecosystem-accordion__right--large <?= $lower_current_key===0 ? 'ecosystem-accordion__right--active' : ''; ?>" data-parent="{{$position_key}}">
              @foreach($sections[$position_key]['boxes'] as $key =>$box )
                <div id="box-{{$key}}-{{$position_key}}"
                  class="ecosystem-accordion__right__course <?= $key===0 ? 'ecosystem-accordion__right__course--active' : ''; ?> ecosystem-accordion__right__course--1">
                  <div class="ecosystem-accordion__right__course__bottom">
                    <article class="course-article">
                      <h6 class="course-ah--4">{!! $box['box_right']['title'] !!}</h6>
                      <div class="line"></div>
                      <p class="course-p--1">{!! $box['box_right']['paragraph'] !!}</p>
                    </article>
                  </div><!-- /.custom-accordion__right__course__top-->
                </div>
              @endforeach
            <!-- END BOXES -->
          </div><!-- /.custom-accordion__right-->
          @php
            $lower_current_key++;
          @endphp
        @endforeach
        </div><!-- /.white-box-->
      </div>
    </div>
  </div>
</section><!-- /.ACCORDION   -->


{{-- PARTNERS  --}}
@component('components.partners',
['partners' => get_field('partners'),
'specialClass' => 'institution',
])@endcomponent


{{-- FOOTER FORM  --}}
@component('components.footer-form',
['footer_form' => get_field('footer_form'),
'specialClass' => 'institution',
])@endcomponent


@endsection
