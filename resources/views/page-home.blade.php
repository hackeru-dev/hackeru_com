{{--
  Template Name: Home Template
--}}

@extends('layouts.app')

@section('content')

<!-- Section Hero -->
<?php
  $hero = get_field('hero');
?>

<section class="pages-hero">
  <div class="pages-hero__container container">
      <div class="pages-hero__headlines">
          <h2 class="pages-hero-h--1">{!! $hero['headline_1'] !!}</h2>
          <h1 class="pages-hero-h--2">{!! $hero['headline_2'] !!}</h1>
          <div class="line"></div>
          <p class="pages-hero-p--1">{!! $hero['paragraph'] !!}</p>
      </div>
  </div>
</section>

<!--
  #=================CAROUSEL===================#
-->
@component('components.carouselPrograms',
['carousel' => get_field('carousel'),
'specialClass' => 'home'
])@endcomponent

<!--
#=================ACADEMIC ACCORDION===================#
-->
<?php
  $academic = get_field('academic');
?>

<section class="academic">
  <div class="academic__container container">
        <article class="academic__headlines">
          <h2 class="academic-ah--1">{!! $academic['headline'] !!}</h2>
          <div class="line"></div>
          <p class="academic-p--1">{!! $academic['paragraph'] !!}</p>
      </article>
      @if($academic['add_mobile_image'] && $academic['mobile_image'])
        <img class="academic__mobile-img" src="{!! $academic['mobile_image'] !!}" alt="bar">
      @endif
    <div class="academic__row">
      <div class="academic-white-box">
      <div class="academic-accordion__left accordion-parent-node" data-position="top">

          <p class="academic-accordion__left-p--1" id="toIgnore">{!! $academic['paragraph_2'] !!}</p>
          <div class="academic-accordion__left__wrapper accordion-wrapper-node">
            <div class="academic-white-box__arrow">
                <img class="academic-icon icon--up" id="academic_btn_up" src="{!! $academic['icon_up'] !!}" alt="arrow up">
                <img class="academic-icon icon--down" id="academic_btn_down" src="{!! $academic['icon_down'] !!}" alt="arrow down">
            </div>

              <div class="academic-white-box__titles accordion-wrapper-node" >
                  @foreach($academic['boxes'] as $key =>$box )
                    <h5 data-id="box-{{$key}}-top"
                      class="academic-accordion__left__course <?= (int)$key===0 ? 'academic-accordion__left__course--active' : ''; ?> academic-accordion__left__course--1>">
                        {!! $box['box_left']['title'] !!}
                    </h5>
                  @endforeach
            </div>
          </div>
        </div><!--/academic-accordion__left-->

        <!-- RIGHT -->
        <div class="academic-accordion__right academic-accordion__right--large">
            @foreach($academic['boxes'] as $key =>$box )
              <div id="box-{{$key}}-{{$position ?? 'top'}}"
                class="academic-accordion__right__course <?= $key===0 ? 'academic-accordion__right__course--active' : ''; ?> academic-accordion__right__course--1">
                  <figure class="academic-accordion__right__course--img">
                      <img class="academic-right-image" src="{!! $box['box_right']['image'] !!}" alt="laptop">
                      <div class="academic-right-overlay"></div>
                  </figure>
              </div>
            @endforeach

          <!-- END BOXES -->
        </div><!-- /.academic-accordion__right-->
      </div>
    </div>
  </div>
</section><!-- /.ACCORDION   -->


{{-- PARTNERS  --}}
@component('components.partners',
['partners' => get_field('partners'),
'specialClass' => 'home',
])@endcomponent


{{-- COLLAGE  --}}
@component('components.collage',
['collage' => get_field('collage'),
'specialClass' => 'home',
])@endcomponent


{{-- Success arousel --}}
@component('components.successCarousel',
['successCarousel' => get_field('successCarousel'),
'specialClass' => 'home',
])@endcomponent


<!--
#=================ACCORDION===================#
-->

@component('components.accordion',
['accordion' => get_field('accordion'),
'specialClass' => 'home',
])@endcomponent


<!--
  #=================ECOSYSTEM===================#
-->
@component('components.ecosystemCarousel',
 ['ecosystemCarousel' => get_field('ecosystemCarousel'),
 'specialClass' => 'home',
 ])@endcomponent
<!--
  #================= FOOTER FORM ===================#
-->
@component('components.footer-form',
['footer_form' => get_field('footer_form'),
'specialClass' => 'home',
])@endcomponent



@endsection
