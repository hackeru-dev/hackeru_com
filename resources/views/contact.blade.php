{{--
  Template Name: Contact Us
--}}

@extends('layouts.app')

@section('content')

<?php
  $heroContact = get_field('hero');
?>
<section class="hero-contact">
    <div class="hero-contact__container container">
        <div class="hero-contact__headlines">
            <h1 class="hero-contact-h--2">{!! $heroContact['headline'] !!}</h1>
            <div class="line"></div>
        </div>
    </div>
</section>

{{-- COLLAGE  --}}
@component('components.collage',
['collage' => get_field('collage'),
'specialClass' => 'contactUs',
])@endcomponent


{{-- Accordion --}}
@component('components.accordion',
['accordion' => get_field('accordion'),
'specialClass' => 'contactUs',
])@endcomponent


{{-- Footer Form --}}
@component('components.footer-form',
['footer_form' => get_field('footer_form'),
'specialClass' => 'contactUs',
])@endcomponent

@endsection
