{{--
  Template Name: Thanks Page
--}}

@extends('layouts.app')

@section('content')

<section class="thanks-hero">
    <div class="cross"></div>
</section>

<section class="thanks-content">
    <img class="thanks-content__triangles" src="@asset('images/home/collageCorners_down.svg')" alt="laptop">
    <div class="cross"></div>
    <div class="container">
      <article class="thanks-content__headlines">
        <h1 class="thanks-content--h--2">Thank You <br> For Contacting Us!</h1>
        <div class="line"></div>
        <h3 class="thanks-content-ah--3">One of our representatives will get back to you within 2-4 business days.</h3>
      </article>
    </div>
</section>

@endsection
