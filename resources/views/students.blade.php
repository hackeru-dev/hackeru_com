{{--
  Template Name: Students
--}}

@extends('layouts.app')

@section('content')

<?php
  $heroStd = get_field('hero');
?>
<section class="hero-students">
  <div class="hero-students__container container">
      <div class="hero-students__headlines">
        <h1 class="hero-students-h--5">{!! $heroStd['headline_1'] !!}</h1>
          <div class="line"></div>
          <h3 class="hero-students-ah--3">{!! $heroStd['headline_2'] !!}</h3>
      </div>
  </div>
</section>

{{-- PARTNERS  --}}
@component('components.partners',
['partners' => get_field('partners'),
'specialClass' => 'students',
])@endcomponent


{{-- New Jobs Section --}}
@component('components.newJobs',
['newJobs' => get_field('newJobs'),
'specialClass' => 'students',
])@endcomponent

<!--
  #=================CAROUSEL PROGRAMS===================#
-->
@component('components.carouselPrograms',
['carousel' => get_field('carousel'),
'specialClass' => 'students'
])@endcomponent

{{-- Testimonials  --}}
@component('components.testimonials',
['testimonials' => get_field('testimonials'),
'specialClass' => 'students'
])@endcomponent

@endsection
