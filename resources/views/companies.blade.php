{{--
  Template Name: Companies
--}}

@extends('layouts.app')

@section('content')

<?php
  $heroComp = get_field('hero');
?>
<section class="hero-companies">
  <div class="hero-companies__container container">
      <div class="hero-companies__headlines">
          <h1 class="hero-companies-h--5">{!! $heroComp['headline_1'] !!}</h1>
          <div class="line"></div>
          <h3 class="hero-companies-ah--3">{!! $heroComp['headline_2'] !!}</h3>
      </div>
  </div>
</section>


{{-- Services Section--}}
<?php
  $services = get_field('services');
?>
<section class="services">
  <img class="services__triangles-left" src="{!! $services['image'] !!}" alt="x">

  <div class="services__container container">
    <article class="services__articles">
        <h2 class="services--ah--1">{!! $services['headline'] !!}</h2>
        <div class="line"></div>
        <p class="services--p--1">{!! $services['paragraph'] !!}</p>
    </article>

    <div class="services__wrapper">
    @foreach($services['boxes'] as $key => $box)
      <div class="services__wrapper__box">
        <p class="box--p--2">{!! $box['title'] !!}</p>
      </div>
    @endforeach
    </div>
  </div>
</section>

{{-- Services Section End--}}

{{-- Provider Section--}}
<?php
  $provider = get_field('provider');
?>

<section class="provider">
  <div class="provider__container container">
      <div class="cross"></div>
      <img class="provider__image-mobile" src="{!! $provider['image_mobile'] !!}" alt="corners">

      <article class="provider__articles">
        <h2 class="provider--ah--1">{!! $provider['headline'] !!}
        <span class="provider-span--ah--3">{!! $provider['marker'] !!}</span>
        </h2>
        <div class="line"></div>
        <p class="provider--p--1">{!! $provider['paragraph'] !!}</p>
    </article>

      {{-- Right --}}
    <div class="provider__row">
      <div class="provider__left">
        <h3 class="provider__left--ah--3">{!! $provider['headline_left'] !!}</h3>
          <!-- Swiper -->
          <div class="swiper-provider swiper-container">
            <div class="swiper-wrapper">
              @foreach($provider['boxes_left'] as $key => $box)
                  <div class="swiper-slide">
                    <div class="provider-slide">
                      <p class="provider-slide--layer">{!! $box['title_1'] !!}</p>
                      <p class="provider-slide--title"> {!! $box['title_2'] !!}</p>
                      <div class="provider-slide--line line"></div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
      </div>

      {{-- Left --}}
      <div class="provider__right">
          <h3 class="provider__right--ah--3">{!! $provider['headline_right'] !!}</h3>
          <div class="line"></div>
            @foreach($provider['boxes_right'] as $key => $box)
              <div class="right-box">
                  <img class="right-box--icon" id="academic_btn_up" src="{!! $box['icon'] !!}" alt="icon">
                  <p class="right-box--p--2">{!! $box['title'] !!}</p>
              </div>
            @endforeach
      </div>
    </div>
  </div>
</section>
{{-- Provider Section End--}}

{{-- Testing Section --}}
<?php
  $testing = get_field('testing');
?>
<section class='testing'>
  <img class="testing__corner_icon_upper" src="{!! $testing['image_triangles'] !!}" alt="corners">

  <div class="testing__container container">
    <article class="testing__articles">
        <h2 class="testing--ah--1">{!! $testing['headline_1'] !!}</h2>
        <div class="line"></div>
        <p class="testing--p--1">{!! $testing['paragraph_1'] !!}</p>
        <h3 class="testing--ah--3">{!! $testing['headline_2'] !!}</h3>
        <p class="testing--p--1">{!! $testing['paragraph_2'] !!}</p>
    </article>

    <div class="testing__wrapper">
        @foreach( $testing['boxes'] as $key => $box)
          <div class="testing-box">
            <img class="testing-box--icon" id="academic_btn_up" src="{!! $box['icon'] !!}" alt="icon">
            <h4 class="testing-box-h--4">{!! $box['title'] !!}</h4>
            <div class="line"></div>
            <p class="testing-box--p--1">{!! $box['paragraph'] !!}</p>
        </div>
        @endforeach
    </div>
  </div>
</section>
{{-- Testing Section End--}}

<!--
  #=================ECOSYSTEM===================#
-->
@component('components.ecosystemCarousel',
 ['ecosystemCarousel' => get_field('ecosystemCarousel'),
 'specialClass' => 'companies',
 ])@endcomponent

{{-- Success arousel --}}
@component('components.successCarousel',
['successCarousel' => get_field('successCarousel'),
'specialClass' => 'companies',
])@endcomponent

{{-- Testimonials  --}}
@component('components.testimonials',
['testimonials' => get_field('testimonials'),
'specialClass' => 'companies'
])@endcomponent

<!--
  #================= FOOTER FORM ===================#
-->
@component('components.footer-form',
['footer_form' => get_field('footer_form'),
'specialClass' => 'companies',
])@endcomponent

@endsection
