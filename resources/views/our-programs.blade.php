{{--
  Template Name: Our Programs
--}}

@extends('layouts.app')

@section('content')

{{-- HERO SECTION --}}
<?php
  $heroPrograms = get_field('hero');
?>
<section class="hero-ourPrograms">
  <div class="hero-ourPrograms__container container">
    <div class="hero-ourPrograms__headlines">
      <div class="cross"></div>
      <h1 class="hero-ourPrograms-h--2">{!! $heroPrograms['headline_1'] !!}</h1>
      <div class="line"></div>
      <h3 class="hero-ourPrograms-ah--3">{!! $heroPrograms['headline_2'] !!}</h3>
      <div class="triangle triangle__red"></div>
    </div>
  </div>
</section>

{{-- Our Programs Image --}}
<?php
  $ourPrograms_image = get_field('ourPrograms_image');
?>
<section class="ourPrograms-image">
  <div class="ourPrograms-image__container container">
    <h2 class="ourPrograms-image-ah--1">{!! $ourPrograms_image['headline'] !!}</h2>
    <div class="line"></div>

    <div class="ourPrograms-image__images">
      <img class="ourPrograms-image__images--corners_icon_left" src="{!! $ourPrograms_image['triangles_img'] !!}"
        alt="triangles">
      <img class="ourPrograms-image__images--bar-icon-right" src="{!! $ourPrograms_image['bar_img'] !!}" alt="barp">
    </div>
  </div>
</section>

{{-- Brain --}}
<?php
  $brain = get_field('brain');
?>

<section class="ourPrograms-brain">
    <div class="ourPrograms-brain__container container">
          <div class="cross"></div>
          <div class="ourPrograms-brain__row">
            @foreach($brain['boxes'] as $key => $box)
              <div class="ourPrograms-brain__box">
                  <h3 class="ourPrograms-brain__box--ah--4">{!! $box['title'] !!}</h3>
                  <div class="line"></div>
                  <p class="ourPrograms-brain__box--p--1">{!! $box['paragraph'] !!}</p>
              </div>
            @endforeach
          </div>
    </div>
</section>

<!--
  #=================CAROUSEL PROGRAMS===================#
-->

<!--
  #=================CAROUSEL===================#
-->
@component('components.carouselPrograms',
['carousel' => get_field('carousel'),
'specialClass' => 'programs'
])@endcomponent


{{-- Success arousel --}}
@component('components.successCarousel',
 ['successCarousel' => get_field('successCarousel'),
 'specialClass' => 'programs'
 ])@endcomponent


{{-- Testimonials  --}}
@component('components.testimonials',
['testimonials' => get_field('testimonials'),
'specialClass' => 'programs'
])@endcomponent


{{-- PARTNERS  --}}
@component('components.partners',
['partners' => get_field('partners'),
'specialClass' => 'programs',
])@endcomponent

<!--
  #================= FOOTER FORM ===================#
-->
@component('components.footer-form',
['footer_form' => get_field('footer_form'),
'specialClass' => 'programs',
])@endcomponent

@endsection
