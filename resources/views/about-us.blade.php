{{--
  Template Name: About US
--}}

@extends('layouts.app')

@section('content')

<?php
  $heroAbout = get_field('hero');
?>
<section class="hero-about">
  <div class="hero-about__container container">
    <img class="hero-about__image-mobile" src="{!! $heroAbout['icon_mobile']!!}" alt="laptop">
    <div class="hero-about__row">
        <div class="hero-about__left-image"></div>

        <div class="hero-about__headlines">
            <h1 class="hero-about-h--2">{!! $heroAbout['headline_1'] !!}</h1>
            <div class="line"></div>
            <h2 class="hero-about-h--1">{!! $heroAbout['headline_2'] !!}  </h2>
            <p class="hero-about-p--1">{!! $heroAbout['paragraph'] !!}</p>

            <div class="hero-about__buttons">
            @foreach($heroAbout['buttons'] as $key => $button)
              <button class="btn btn--2">{!! $button['cta'] !!}</button>
            @endforeach
            </div>
        </div>
    </div>
  </div>
</section>

{{-- Story --}}
@component('components.story',
 ['story' => get_field('story'),
 'specialClass' => 'aboutStory',
 ])@endcomponent


<!--
#=================ACCORDION===================#
-->
@component('components.accordion',
['accordion' => get_field('accordion'),
'specialClass' => 'about',
])@endcomponent


{{-- Success arousel --}}
@component('components.successCarousel',
['successCarousel' => get_field('successCarousel'),
'specialClass' => 'about',
])@endcomponent


 {{-- Testimonials  --}}
@component('components.testimonials',
['testimonials' => get_field('testimonials'),
'specialClass' => 'about'
])@endcomponent


{{-- PARTNERS  --}}
@component('components.partners',
['partners' => get_field('partners'),
'specialClass' => 'about',
])@endcomponent


<!--
  #================= FOOTER FORM ===================#
-->
@component('components.footer-form',
['footer_form' => get_field('footer_form'),
'specialClass' => 'about',
])@endcomponent

@endsection
