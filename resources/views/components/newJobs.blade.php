{{-- New Jobs Section --}}

<section class="new-jobs {{$specialClass}}">
  {{-- <img class="new-jobs__triangles-mobile" src="@asset('images/home/footer_corners.svg')" alt="triangles"> --}}
  <div class="cross"></div>
  <div class="new-jobs__row">

    <div class="new-jobs-left"></div>

    <div class="new-jobs-right">
      <h3 class="new-jobs-right--ah--6 {{$headlineSpecialClass ?? ''}}">
        @if($newJobs['headlines'])
          @foreach ($newJobs['headlines'] as $headline)
              @if ($headline['type_of_headline']==='normal')
                  {!! $headline['headline'] !!}
              @else
                <span class="new-jobs-right--marker {{$headlineSpecialClass ?? ''}}">{!! $headline['headline'] !!}</span>
              @endif
          @endforeach
        @endif
      </h3>
        <p class="new-jobs-right--p--1">{!! $newJobs['paragraph'] !!}</p>
        @if($newJobs['add_button'] && $newJobs['cta'])
        <button class="btn btn--2">{!! $newJobs['cta'] !!}</button>
        @endif
    </div>

  </div>
</section>
