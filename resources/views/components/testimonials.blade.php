<section class="testimonials {{$specialClass}}">
  <div class="testimonials__container container">
    <h2 class="testimonials--ah--1">{!! $testimonials['headline'] !!}</h2>
      <div class="line"></div>
      @if($testimonials['add_image'] && $testimonials['image'])
        {{-- <img class="testimonials__bar-icon-left" src="{!! $testimonials['image'] !!}" alt="laptop"> --}}
          <picture>
            <source media="(max-width:578px)" srcset="{!! $testimonials['image_mobile'] !!}">
            <img class="testimonials__bar-icon-left" src="{!! $testimonials['image'] !!}" alt="triangles">
        </picture>
      @endif
  </div>

      <div class="swiper-testimonials swiper-container">
        <div class="swiper-wrapper">

        @foreach($testimonials['boxes'] as $key =>$box )
          <div class="swiper-slide">
            <div class="testimonials__slide">
            <img class="testimonials__slide--photo" src="{!! $box['slide_icon'] !!}" alt="photo">
            <article class="article">
                <div class="testimonials__slide--title">
                  <h3 class="testimonials__slide--ah--4">{!! $box['title'] !!}</h3>
                  @if($box['add_youtube'] && $box['logo_youtube'])
                    <img class="" src="{!! $box['logo_youtube'] !!}" alt="youtube" style="width:5rem">
                  @endif
                  </div>
                <p class="testimonials__slide--p--1">{!! $box['subtitle'] !!}</p>
                <div class="line"></div>
                  <p class="testimonials__slide--p--3">{!! $box['paragraph'] !!}</p>
            </article>
              @if($box['add_logo'] && $box['logo'])
                <img class="testimonials__slide--logo" src="{!! $box['logo'] !!}" alt="logo">
              @endif
            </div>
          </div>
          @endforeach

        </div>
          <!-- Add Pagination -->
        <div class="swiper-scrollbar"></div>
      </div>
</section>
