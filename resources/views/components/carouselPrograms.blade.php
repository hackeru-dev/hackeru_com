<!--
  #=================CAROUSEL===================#
-->
<section class="carousel-programs {{$specialClass}}">
  <div class="carousel-programs__container container">
    @if($carousel['add_headline'] && $carousel['headline_1'])
    <h2 class="carousel-programs-ah--1">{!! $carousel['headline_1'] !!}</h2>
    <div class="line"></div>
    @endif

    @if($carousel['add_right_image'] && $carousel['right_image'])
      <img class="carousel-programs__right-img" src="{!! $carousel['right_image'] !!}" alt="bar">
    @endif

    @if($carousel['add_mobile_image'] && $carousel['mobile_image'])
      <img class="carousel-programs__mobile-img" src="{!! $carousel['mobile_image'] !!}" alt="bar">
    @endif
    <!-- SWIPER COLUMN -->
    <div class="carousel-programs__right">

      <!-- THE CAROUSEL -->
      <div class="swiper-programs swiper-container" style="z-index: 0;">
        <div class="swiper-wrapper">

          @foreach ($carousel['boxes'] as $key=>$box)
            <div class="swiper-slide">
                  <a href="<?= $box['url_link'] ?>" class="slide slide--{{$key+1}}" id="program_slide">
                <div class="slide__top">
                  <img class="slide__img" src="<?= $box['slide_icon'] ?>" alt="slide-icon">
                  <h5 class="slide__title"><?= $box['title'] ?></h5>
                </div>
                <p class="slide__text"><?= $box['paragraph'] ?></p>
                <img class="slide__img" src="<?= $box['slide_icon'] ?>" alt="slide-icon">
                <div class="border_slide"></div>
              </a>
            </div>
          @endforeach

        </div><!-- /.swiper-wrapper-->
        <div class="programs-scrollbar swiper-scrollbar"></div>
      </div><!-- /.swiper-container-->

    </div><!-- /carousel__right-->
  </div><!-- /carousel__row-->
</section><!-- END .carousel-->

