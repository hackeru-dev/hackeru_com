
<!--
#=================ACCORDION===================#
-->
@if($accordion = get_field('accordion'))
<section class="accordion-section {{$specialClass}}">
  <div class="cross cross__blue"></div>
  <div class="triangle"></div>
  <img class="accordion-section__triangles" src="{!! $accordion['triangles_image']!!}" alt="triangles">

  <div class="accordion-section__container container">
    <div class="accordion-section__row">
      <article class="accordion-section__article">
        <h2 class="accordion-section-ah--1">{!! $accordion['headline']!!}</h2>
        <div class="line"></div>
        <p class="custom-accordion-p--1 para-mobile" id="toIgnore">{!! $accordion['paragraph']!!}</p>
      </article>
      <!--
      #=================MOBILE ACCORDION===================#
      -->
      <section class="accordion__mobile">
          @foreach($accordion['boxes'] as $key =>$box )
          <div class="mobile-wrapper">
            <h5 class="accordion__toggle-item"> {!! $box['box_left']['title'] !!}</h5>
            <div class="accordion__content">
              <p class="mobile-para"> {!! $box['box_right']['paragraph'] !!} </p>
            </div>
          </div>
        @endforeach
      </section>
      <!--
      #=================END MOBILE ACCORDION===================#
      -->
      <div class="white-box">
      <div class="custom-accordion__left accordion-parent-node" data-position="bottom">

        @if($accordion['add_paragraph'] && $accordion['paragraph'])
          <p class="custom-accordion-p--1" id="toIgnore">{!! $accordion['paragraph']!!}</p>
        @endif

          @foreach($accordion['boxes'] as $key =>$box )
              <h5 data-id="box-{{$key}}-bottom"
                class="custom-accordion__left__course <?= (int)$key===0 ? 'custom-accordion__left__course--active' : ''; ?> custom-accordion__left__course--1>">
                {!! $box['box_left']['title'] !!}
              </h5>
            @endforeach
        </div><!--/custom-accordion__left-->

        <!-- RIGHT -->
        <div class="custom-accordion__right custom-accordion__right--large">
          <img class="custom-accordion__right--bar_icon_right" src="{!! $accordion['bar_image']!!}" alt="bar">
          <div class="custom-accordion__right__arrow">
                <img class="custom-accordion-icon icon--up" id="accord_btn_up" src="{!! $accordion['icon_up']!!}" alt="arrow up">
                <img class="custom-accordion-icon icon--down" id="accord_btn_down" src="{!! $accordion['icon_down']!!}" alt="arrow down">
            </div>
            @foreach($accordion['boxes'] as $key =>$box )
              <div id="box-{{$key}}-{{$position ?? 'bottom'}}"
                class="custom-accordion__right__course <?= $key===0 ? 'custom-accordion__right__course--active' : ''; ?> custom-accordion__right__course--1">
                <div class="custom-accordion__right__course__top">
                    <img src="{!! $box['box_right']['image'] !!}" alt="Icon">
                </div><!-- /.custom-accordion__right__course__top-->
                <div class="custom-accordion__right__course__bottom">
                  <article class="course-article">
                    <h6 class="course-ah--4">{!! $box['box_right']['title'] !!}</h6>
                    <div class="line"></div>
                    <p class="course-para">{!! $box['box_right']['paragraph'] !!}</p>
                  </article>
                @if($box['box_right']['add_button'] && $box['box_right']['cta'])
                  <button class="btn btn--2">{!! $box['box_right']['cta'] !!}</button>
                @endif
                </div><!-- /.custom-accordion__right__course__top-->
              </div>
            @endforeach

          <!-- END BOXES -->
        </div><!-- /.custom-accordion__right-->
      </div>
    </div>
  </div>
</section><!-- /.ACCORDION   -->
@endif
