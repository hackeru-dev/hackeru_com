
      <div class="numbers">
        @foreach($numbers['boxes'] as $key =>$box )
          <div class="numbers__box" >
            <h3 class="numbers-ah--2"> <span class="numbers-number" data-n="{!! $box['title'] !!}">0</span>{!! $box['title_special_char'] !!}</h3>
            <p class="numbers-p--2">{!! $box['paragraph'] !!}</p>
          </div>
        @endforeach
    </div>
