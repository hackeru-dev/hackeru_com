
<section class="require {{$specialClass}}">
  <div class="require__container container">
        <h2 class="require-ah--1">{!! $require['headline'] !!}</h2>
        <div class="line"></div>
        @if($require['add_paragraph'] && $require['paragraph'])
          <p class="require-p--1"> {!! $require['paragraph'] !!} </p>
        @endif
        <div class="cross"></div>
        @if($require['add_image'] && $require['image'])
          <img class="require__icon-right" src="{!! $require['image'] !!}" alt="laptop">
        @endif

        @if($require['add_image_mobile'] && $require['image_mobile'])
          <img class="require__image-mobile" src="{!! $require['image_mobile'] !!}" alt="x">
        @endif

        <div class="require__white-boxes">
            @foreach($require['boxes'] as $key => $box)
              <div class="box">
                <p class="box-p--1">{!! $box['title']!!}</p>
                <div class="line"></div>
              </div>
            @endforeach
        </div>

      @if($require['add_note'])
        <div class="require__note">
            <div class="cross"></div>
            <h2 class="require__note--h--1">{!! $require['paragraph_note'] !!}</h2>
              @if($require['add_button_note'] && $require['cta'])
                <button class="require__note--btn btn btn--2">{!! $require['cta'] !!}</button>
              @endif
        </div>
      @endif
  </div>
</section>
