<section class="story {{$specialClass}}">
  <div class="cross"></div>
    @if($story['add_image'] && $story['icon_bar'])
        <picture>
          <source media="(max-width:578px)" srcset="{!! $story['icon_mobile'] !!}">
          <img class="story__images story__images--bar-icon-left" src="{!! $story['icon_bar'] !!}" alt="laptop">
        </picture>
    @endif
  <div class="story__container container">
      <article class="story__articles">
          <h2 class="story--ah--1">{!! $story['headline'] !!}</h2>
          <div class="line"></div>
      </article>


      <div class="story__row">
        <div class="story__row__left">
        @foreach($story['paragraphs'] as $key => $para)
            <p class="story__row__left--p--1"> {!! $para['paragraph'] !!} </p>
        @endforeach
        </div>
        <div class="story__row__right">
            <figure class="story__row__right--figure">
                <img class="story-right-image" src="@asset('images/about/story_image.jpg')" alt="x">
                <div class="story-right-overlay"></div>
            </figure>
        </div>
      </div>
    </div>
</section>
