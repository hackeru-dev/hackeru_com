
<!--
#=================ACCORDION===================#
-->
<section class="accordion-ecosystem {{$specialClass ?? ''}}">
  <div class="accordion-ecosystem__container container">
    <div class="accordion-ecosystem__row">
      <article class="accordion-ecosystem__article">
        <h2 class="accordion-ecosystem-ah--1">Building Technological <br> Ecosystems</h2>
        <div class="line"></div>
      </article>
      <!--
      #=================MOBILE ACCORDION===================#
      -->
      <section class="accordion__main accordion__main--programs">
          <h5 class="accordion__toggle-item accordion__toggle-item--programs"></h5>
          <div class="accordion__content accordion__content_overview"></div>
      </section>
      <!--
      #=================END MOBILE ACCORDION===================#
      -->



      <div class="white-box">
          <img class="white-box--bar_icon_right" src="@asset('images/home/footer_icon_left.svg')" alt="laptop">



      <div class="ecosystem-accordion__left accordion-parent-node" data-position="top">
          <div class="ecosystem-accordion__left__buttons">
              <button class="ecosystem-btn btn--2 ecosystem-btn--active" id="local_btn" data-popup="accordion_right--1">From Local</button>
          </div>

            @foreach(['inna', 'or', 'arthur', 'or'] as $key => $value)
              <h5 data-id="box-{{$key}}-top"
                class="ecosystem-accordion__left__course mobile <?= (int)$key===0 ? 'ecosystem-accordion__left__course--active mobile' : ''; ?> ecosystem-accordion__left__course--1>">
                On The Global Level
              </h5>
            @endforeach
        </div><!--/custom-accordion__left-->

          <div class="ecosystem-accordion__left accordion-parent-node" data-position="top">
          <div class="ecosystem-accordion__left__buttons">
              <button class="ecosystem-btn btn--2" id="global_btn" data-popup="accordion_right--1">To Global</button>
          </div>

            @foreach(['inna', 'or', 'arthur', 'or'] as $key => $value)
              <h5 data-id="box-{{$key}}-top"
                class="ecosystem-accordion__left__course <?= (int)$key===0 ? 'ecosystem-accordion__left__course--active' : ''; ?> ecosystem-accordion__left__course--1>">
                On The Global Level
              </h5>
            @endforeach
        </div><!--/custom-accordion__left-->


        <!-- RIGHT -->
        <div class="ecosystem-accordion__right ecosystem-accordion__right--large">

            @foreach(['inna', 'or', 'arthur', 'or', 'or', 'or', 'or', 'or'] as $key => $value)
              <div id="box-{{$key}}-{{$position ?? 'top'}}"
                class="ecosystem-accordion__right__course <?= $key===0 ? 'ecosystem-accordion__right__course--active' : ''; ?> ecosystem-accordion__right__course--1">
                <div class="ecosystem-accordion__right__course__bottom">
                  <article class="course-article">
                    <h6 class="course-title">On The Global Level {{$key}}</h6>
                    <div class="line"></div>
                    <p class="course-p--1">Diminish the global workforce deficit by training the next generation of dugutal and cyber experts to build sustainable technology ecosystem around the globe.</p>
                  </article>
                </div><!-- /.custom-accordion__right__course__top-->
              </div>
            @endforeach

          <!-- END BOXES -->
        </div><!-- /.custom-accordion__right-->

          <!-- END BOXES -->
        </div><!-- /.custom-accordion__right-->
      </div>
    </div>
  </div>
</section><!-- /.ACCORDION   -->

