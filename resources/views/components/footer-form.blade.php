<!--
  #================= FOOTER FORM ===================#
-->
@if($footer_form = get_field('footer_form'))
<section class="footer-form {{$specialClass}}">

  @if($footer_form['add_triangles_image'] && $footer_form['triangles_image'])
  <img class="footer-form__icon footer-form__icon--corner_icon_upper" src="{!!$footer_form['triangles_image'] !!}" alt="corners">
  @endif

  <img class="footer-form__icon footer-form__icon--bar-down-mob" src="{!!$footer_form['bar_image'] !!}" alt="bar">

  <div class="footer-form__container container">
    <div class="triangle"></div>
    <div class="cross"></div>
    <div class="cross cross__dark-blue"></div>
    <article class="footer-form__article">
        <h2 class="footer-form-ah--1">{!!$footer_form['headline'] !!}</h2>
        <div class="line"></div>
    </article>

    <div class="footer-form__row">

    {{-- Left --}}
        <div class="footer-form__row--left">
        {{-- Grid Form --}}
            <div class="form-container">
              <!-- Form -->
              <form class="grid-form">
                <!-- Full Name -->
                <div class="grid-form__group grid-form__fullname">
                  <label class="grid-form__label">Full Name</label>
                  <input class="grid-form__control" type="text" name="fullname" placeholder="Full Name" />
                </div>

                <!-- Email -->
                <div class="grid-form__group grid-form__email">
                    <label class="grid-form__label">Email</label>
                    <input class="grid-form__control" type="email" name="email" placeholder="Email" />
                </div>

                  <!-- Country -->
                <div class="grid-form__group grid-form__country">
                    <label class="grid-form__label">Country</label>
                    <input class="grid-form__control" type="text" name="country" placeholder="Israel" />
                </div>

                <!-- Company -->
                <div class="grid-form__group grid-form__company">
                    <label class="grid-form__label">Company</label>
                    <input class="grid-form__control" type="text" name="company" placeholder="HackerU" />
                </div>

                <!-- Phone -->
                <div class="grid-form__group grid-form__phone">
                  <label class="grid-form__label">Phone</label>
                  <input class="grid-form__control" type="tel" name="phone" placeholder="Phone" />
                </div>

                <!-- Title -->
                <div class="grid-form__group grid-form__company">
                    <label class="grid-form__label">Title</label>
                    <input class="grid-form__control" type="text" name="title" placeholder="CTO" />
                </div>

                <!-- Message -->
                <div class="grid-form__group grid-form__msg">
                    <label class="grid-form__label">Message</label>
                    <textarea class="grid-form__control" name="msg" id="" cols="30" rows="10" placeholder="Comment Box"></textarea>
                </div>

                <div class="grid-form__group grid-form__disclaimer">
                <p class="disclaimer">
                    {!!
                    App::disclaimer( $footer_form['cta'] )
                  !!}
                </p>
                </div>
            </form>
            </div><!-- /.form-container -->

            <button class="btn btn--2">{!!$footer_form['cta'] !!}</button>
        </div>

    {{-- Right --}}
        <div class="footer-form__row--right">
            <div class="cross plus"></div>
            <figure class="footer-form__img">
              <img class="footer-form__img--image" src="{!!$footer_form['right_image'] !!}" alt="buildings">
          </figure>
            <img class="footer-form__bar_icon_down" src="{!!$footer_form['bar_image'] !!}" alt="bar">
        </div>
    </div>
  </div>
</section>
@endif
