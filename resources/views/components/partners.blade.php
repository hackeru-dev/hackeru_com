
{{-- PARTNERS  --}}
<section class="partners {{$specialClass}}">
  <div class="partners__container container">
      <div class="cross cross__blue"></div>
    @if($partners['add_image'] && $partners['image'])
      <img class="partners__bar-icon-left" src="{!! $partners['image'] !!}" alt="bar">
    @endif

    @if($partners['add_image_mobile'] && $partners['image_mobile'])
      <img class="partners__mobile-img" src="{!! $partners['image_mobile'] !!}" alt="bar">
    @endif

    <div class="partners__article">
      <h2 class="partners__article-ah--1">{!! $partners['headline'] !!}</h2>
      <div class="line"></div>
    </div>

      <div class="swiper-main">
          <!-- Swiper -->
          <div class="swiper-test swiper-container">
            <div class="swiper-wrapper">
              @foreach($partners['slides'] as $key =>$box )
                <div class="swiper-slide">
                  <div class="partners-slide">
                      <img src="{!! $box['slide_image'] !!}" alt="Trulli" class="partners-slide--img" >
                  </div>
                </div>
              @endforeach

            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next swiper-button-next1"></div>
            <div class="swiper-button-prev swiper-button-prev1"></div>
          </div>

      </div>
  </div>
</section>
