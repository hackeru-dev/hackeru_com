<section class="ecosystem {{$specialClass ?? ''}}">
  <div class="ecosystem__container container">
    <div class="ecosystem__row">
        <div class="ecosystem__left">
            <h2 class="ecosystem-ah--1">{!! $ecosystemCarousel['headline'] !!}</h2>
            <div class="line"></div>
            <p class="ecosystem-p--1">{!! $ecosystemCarousel['paragraph'] !!}</p>
            @if($ecosystemCarousel['add_button'] && $ecosystemCarousel['cta'])
              <button class="btn btn--2">{!! $ecosystemCarousel['cta'] !!}</button>
            @endif
        </div>
    <div class="ecosystem__right">
              <div class="swiper-ecosystem swiper-container">
                  <div class="swiper-wrapper">

                  @foreach($ecosystemCarousel['boxes'] as $key =>$box )
                    <div class="swiper-slide">
                      <div class="ecosystem-slide">
                          <div class="article-content">
                            <img src="{!! $box['slide_icon'] !!}" alt="Trulli" class="ecosystem-slide--image" >
                            <p class="ecosystem-slide-para">{!! $box['title'] !!}</p>
                          </div>
                      </div>
                    </div>
                  @endforeach
                  </div>
                <div class="swiper-pagination"></div>
            </div>
            <button class="btn btn--2 ">{!! $ecosystemCarousel['cta'] !!}</button>
        </div>
        <!-- /right-->
    </div>
  </div>
</section>
