{{-- SUCCESS CAROUSEL  --}}
<section class="success-carousel {{$specialClass}}">
  <div class="success-carousel__container container">
      @if($successCarousel['headline'] && $successCarousel['headline'])
        <h2 class="success-carousel--ah--1">{!! $successCarousel['headline'] !!}</h2>
        <div class="line"></div>
      @endif
    </div>

      <div class="swiper-success swiper-container">
          <div class="swiper-wrapper">
          @foreach($successCarousel['boxes'] as $key =>$box )
            <div class="swiper-slide">
                <div class="success-carousel__slide success-carousel__slide--{{$key+1}}" style="background-image: linear-gradient(rgba(255, 0, 0, 0), rgba(255, 0, 0, 0)), url({!! $box['bg_image'] !!});">
                    <h5 class="success-carousel__slide__title">{!! $box['title'] !!}</h5>
                      <div class="line line__white"></div>
                      <p class="success-carousel__slide__text">{!! $box['paragraph'] !!}</p>
                </div>
            </div>
          @endforeach
    </div>
          <!-- Add Pagination -->
        <div class="swiper-scrollbar"></div>
</section>
