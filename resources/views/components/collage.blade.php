{{-- COLLAGE  --}}
@if($collage = get_field('collage'))
<section class="collage {{$specialClass ?? ''}}">
  <img class="collage__icon collage__icon--corners_icon_down" src="{!! $collage['image_triangles'] !!}')"
    alt="triangles">
  <img class="collage__icon collage__icon--bar-icon-right" src="{!! $collage['image_bar'] !!}" alt="bar">

  <div class="collage__container container">
    <div class="cross"></div>
    <div class="triangle"></div>

      <div class="collage__wrapper">

  @foreach ($collage['boxes'] as $key=>$box)
      <div class="box box--{{$key+1}} {{($box['add_blur'] && $box['add_blur'] === true) ? 'box--blur' : ''}}">
        <article class="article">
          <h3 class="article-ah--1">{!! $box['title'] !!}</h3>
          <div class="line line__white"></div>
            @if($box['add_paragraph'] && $box['paragraph'])
              <p class="article-p--1">{!! $box['paragraph'] !!}</p>
            @endif
            @if($box['add_button'] && $box['cta'])
              <button class="btn btn--2">{!! $box['cta'] !!}</button>
            @endif
        </article>
      </div>
  @endforeach

    </div>
  </div>
</section>
@endif
