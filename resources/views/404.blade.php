@extends('layouts.app')

@section('content')

  @if (!have_posts())
      <section class="hero-404">
          <div class="cross"></div>
      </section>

      <section class="content-404">
          <img class="content-404__triangles" src="@asset('images/home/collageCorners_down.svg')" alt="laptop">
          <img class="content-404__bar" src="@asset('images/home/home_icon_bar.svg')" alt="laptop">
          <div class="cross"></div>
          <div class="container">
            <article class="content-404__headlines">
              <h1 class="content-404--h--2">Oops!</h1>
              <div class="line"></div>
              <h3 class="content-404-ah--3">Looks like this page is still under construction. Come back soon!</h3>
            </article>
          </div>
      </section>

      <!--
  #================= FOOTER FORM ===================#
-->
@component('components.footer-form',
['footer_form' => $footer_form,
'specialClass' => 'page404',
])@endcomponent
  @endif
@endsection
