<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{

    public static $socialIcons = [
        'facebook',
        'linkedin',
        'twitter'
    ];

    public static $footerUrls = [
        'hackerusa.com',
        'hackerupro.com',
        'hackeruge.com',
        'hackeru.co.il',
        'hackerusolutions.com'
    ];

    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function disclaimer($ctaText = null, $returnValue = false) {
        if($ctaText && strlen($ctaText)){
            if ($returnValue) {
                return preg_replace('/Submit/i', $ctaText, get_theme_mod( 'disclaimer' ));
            } else {
                echo preg_replace('/Submit/i', $ctaText, get_theme_mod( 'disclaimer' ));
            }
        }else {
            return get_theme_mod( 'disclaimer' );
        }
    }
}
