<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$students= new FieldsBuilder('students');

$students
    ->setLocation('page_template', '==', 'views/students.blade.php');

$students
    ->addTab('# Hero #')
    ->addGroup('hero', ['label' => 'Hero'])
        ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTextarea('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
    ->endGroup();

$students
    ->addTab('# Partners  #')
        ->addFields(get_field_partial('components.partners'));

$students
    ->addTab('# New Jobs  #')
        ->addFields(get_field_partial('components.newJobs'));

$students
    ->addTab('# Carousel Programs  #')
        ->addFields(get_field_partial('components.carouselPrograms'));

$students
    ->addTab('# Testimonials  #')
        ->addFields(get_field_partial('components.testimonials'));


return $students;
?>
