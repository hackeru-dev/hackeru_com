<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$institution= new FieldsBuilder('institution');

$institution
    ->setLocation('page_template', '==', 'views/academic-institutions.blade.php');

$institution
    ->addTab('# Hero #')
    ->addGroup('hero', ['label' => 'Hero'])
        ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addText('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
        ->addText('headline_3', ['label' => 'Headline 3','default_value' => 'Lorem Ipsum is simply dummy text of the',])
        ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
    ->endGroup();

$institution
    ->addTab('# Gap  #')
        ->addGroup('gap', ['label' => 'Gap'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addImage('triangles_img', [
                'label' => 'Icon Triangles',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addImage('triangles_img_mob', [
                'label' => 'Icon Triangles Mobile',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addImage('bar_img_mob', [
                'label' => 'Icon Bar Mobile',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addFields(get_field_partial('components.numbers'))
        ->endGroup();


$institution
->addTab('# Require-1 #')
    ->addGroup('require_1', ['label' => 'Require 1'])
        ->addFields(get_field_partial('components.require'))
    ->endGroup();

$institution
->addTab('# Require-2 #')
    ->addGroup('require', ['label' => 'Require'])
        ->addFields(get_field_partial('components.require'))
    ->endGroup();

$institution
    ->addTab('# Ecosystem Accordion  #')
        ->addGroup('ecosystem_accordion', ['label' => 'Ecosystem Accordion'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addGroup('sections', ['label' => 'sections'])
                ///// Local Box
                ->addGroup('local', ['label' => 'Local'])
                    ->addRepeater('boxes', ['label' => 'Slides Boxes'])
                        ->addGroup('box_left', ['label' => 'Box Left'])
                            ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                        ->endGroup()

                        ->addGroup('box_right', ['label' => 'Box Right'])
                            ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
                        ->endGroup()
                    ->endRepeater()
                ->endGroup()
                /// END of Local Box

                ///// Global Box
                ->addGroup('global', ['label' => 'Global'])
                    ->addRepeater('boxes', ['label' => 'Slides Boxes'])
                        ->addGroup('box_left', ['label' => 'Box Left'])
                            ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                        ->endGroup()

                        ->addGroup('box_right', ['label' => 'Box Right'])
                            ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
                        ->endGroup()
                    ->endRepeater()
                ->endGroup()
                /// END of Global Box
            ->endGroup()
        ->endGroup();

$institution
    ->addTab('# Partners  #')
        ->addFields(get_field_partial('components.partners'));

$institution
    ->addTab('# Footer Form #')
        ->addFields(get_field_partial('components.footer_form'));

return $institution;
?>
