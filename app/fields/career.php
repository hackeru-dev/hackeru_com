<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$career= new FieldsBuilder('career');

$career
    ->setLocation('page_template', '==', 'views/career.blade.php');

$career
    ->addTab('# Hero #')
        ->addGroup('hero', ['label' => 'Hero'])
            ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
            ->addText('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
        ->endGroup();

$career
    ->addTab('# Career Accordion #')
        ->addGroup('careerAccordion', ['label' => 'Career Accordion'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])

            ->addImage('bar_image', [
                'label' => 'Bar Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addImage('bar_image_mobile', [
                'label' => 'Bar Image Mobile',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addImage('image_mobile', [
                'label' => 'Image Mobile',
                'instructions' => '',
                'required' => 0,
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addImage('icon_up', [
                'label' => 'Icon Up',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addImage('icon_down', [
                'label' => 'Icon Down',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addRepeater('boxes', ['label' => 'Slides Boxes'])
                ->addGroup('box_left', ['label' => 'Box Left'])
                    ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                ->endGroup()

                ->addGroup('box_right', ['label' => 'Box Right'])
                    ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                    ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
                ->endGroup()
            ->endRepeater()
    ->endGroup();

$career
    ->addTab('# New Jobs  #')
        ->addFields(get_field_partial('components.newJobs'));

$career
    ->addTab('# Require #')
        ->addGroup('require', ['label' => 'Require'])
            ->addFields(get_field_partial('components.require'))
        ->endGroup();

$career
    ->addTab('# Specialist #')
        ->addGroup('specialist', ['label' => 'Specialist'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addImage('bar_image', [
                'label' => 'Bar Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addImage('icon_up', [
                'label' => 'Icon Up',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addImage('icon_down', [
                'label' => 'Icon Down',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addRepeater('boxes', ['label' => 'Slides Boxes'])
            ->addGroup('box_left', ['label' => 'Box Left'])
                ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
            ->endGroup()

            ->addGroup('box_right', ['label' => 'Box Right'])
                ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
            ->endGroup()
        ->endRepeater()
    ->endGroup();

$career
    ->addTab('# Numbers Section #')
        ->addRepeater('boxes', ['label' => 'Boxes'])
            ->addText('title', ['label' => 'Title','default_value' => '',])
            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
        ->addRadio('type_of_headline')
            ->addChoices(['number' => 'Number'])
            ->addChoices(['text' => 'Text'])
        ->endRepeater();
return $career;
?>
