<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$contact= new FieldsBuilder('contact');

$contact
    ->setLocation('page_template', '==', 'views/contact.blade.php');

$contact
    ->addTab('# Hero #')
        ->addGroup('hero', ['label' => 'Hero'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->endGroup();

$contact
    ->addTab('# Collage  #')
        ->addFields(get_field_partial('components.collage'));

$contact
    ->addTab('# Accordion #')
        ->addFields(get_field_partial('components.accordion'));

$contact
    ->addTab('# Footer Form #')
        ->addFields(get_field_partial('components.footer_form'));

return $contact;
?>
