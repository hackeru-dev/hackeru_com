<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$require= new FieldsBuilder('require');

$require
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTrueFalse('add_paragraph')
                ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->conditional('add_paragraph', '==', '1')

            ->addTrueFalse('add_note')
                ->addTextarea('paragraph_note', ['label' => 'Paragraph Note', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->conditional('add_note', '==', '1')

            ->addTrueFalse('add_button_note')
                ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])
            ->conditional('add_button_note', '==', '1')

            ->addTrueFalse('add_image')
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->conditional('add_image', '==', '1')

            ->addTrueFalse('add_image_mobile')
            ->addImage('image_mobile', [
                'label' => 'Image Mobile',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->conditional('add_image_mobile', '==', '1')

            ->addRepeater('boxes', ['label' => 'Boxes'])
                ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->endRepeater();
return $require;
?>
