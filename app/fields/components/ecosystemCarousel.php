<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$ecosystemCarousel= new FieldsBuilder('ecosystemCarousel');

$ecosystemCarousel
    ->addGroup('ecosystemCarousel', ['label' => 'ecosystem Carousel'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
        ->addTrueFalse('add_button')
            ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])
        ->conditional('add_button', '==', '1')

        ->addRepeater('boxes', ['label' => 'Boxes'])
            ->addImage('slide_icon', [
                'label' => 'Slide Icon',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
        ->endRepeater()
    ->endGroup();

return $ecosystemCarousel;
?>
