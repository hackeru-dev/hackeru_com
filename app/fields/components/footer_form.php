<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$footer_form = new FieldsBuilder('footer_form');

$footer_form
    ->addGroup('footer_form', ['label' => 'Footer form'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTrueFalse('add_triangles_image')
            ->addImage('triangles_image', [
                'label' => 'Triangles Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_triangles_image', '==', '1')

        ->addImage('bar_image', [
            'label' => 'Bar Image',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])

        ->addImage('bar_image_mobile', [
            'label' => 'Bar Image Mobile',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
        ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])

        ->addImage('right_image', [
            'label' => 'Right Image',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
    ->endGroup();

return $footer_form;
?>
