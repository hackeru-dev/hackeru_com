<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$carouselPrograms= new FieldsBuilder('carouselPrograms');

$carouselPrograms
    ->addGroup('carousel', ['label' => 'carousel'])
        ->addTrueFalse('add_headline')
            ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->conditional('add_headline', '==', '1')

        ->addTrueFalse('add_right_image')
            ->addImage('right_image', [
                'label' => 'Right Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_right_image', '==', '1')

        ->addTrueFalse('add_mobile_image')
            ->addImage('mobile_image', [
                'label' => 'Mobile Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_mobile_image', '==', '1')

        ->addRepeater('boxes', ['label' => 'Boxes'])
                ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
                ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
                ->addImage('slide_icon', [
                    'label' => 'Slide Icon',
                    'return_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ])
                ->addTrueFalse('add_url')
                    ->addUrl('url_link', ['label' => 'Link Url', 'required' => 0,'default_value' => '',])
                ->conditional('add_url', '==', '1')
        ->endRepeater()
    ->endGroup();

return $carouselPrograms;
?>
