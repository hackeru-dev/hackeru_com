<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$collage= new FieldsBuilder('collage');

$collage
    ->addGroup('collage', ['label' => 'Collage'])
        ->addTrueFalse('add_image_triangles')
            ->addImage('image_triangles', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->conditional('add_image_triangles', '==', '1')

            ->addTrueFalse('add_image_bar')
            ->addImage('image_bar', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->conditional('add_image_bar', '==', '1')

        ->addRepeater('boxes', ['label' => 'Boxes'])
            ->addTrueFalse('add_blur')
                ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])

                ->addTrueFalse('add_paragraph')
                    ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
                ->conditional('add_paragraph', '==', '1')

                ->addTrueFalse('add_button')
                    ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])
                ->conditional('add_button', '==', '1')

        ->endRepeater()
    ->endGroup();

return $collage;
?>
