<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$testimonials= new FieldsBuilder('testimonials');

$testimonials
    ->addGroup('testimonials', ['label' => 'testimonials'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTrueFalse('add_image')
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_image', '==', '1')

        ->addTrueFalse('add_image_mobile')
        ->addImage('image_mobile', [
            'label' => 'Image Mobile',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
        ->conditional('add_image_mobile', '==', '1')

        ->addRepeater('boxes', ['label' => 'Boxes'])
            ->addImage('slide_icon', [
                'label' => 'Slide Icon',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addTrueFalse('add_youtube')
            ->addImage('logo_youtube', [
                'label' => 'Logo Youtube',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->conditional('add_youtube', '==', '1')

            ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
            ->addTextarea('subtitle', ['label' => 'Subtitle', 'default_value' => 'Lorem',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addTrueFalse('add_logo')
                ->addImage('logo', [
                    'label' => 'Logo',
                    'return_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ])
            ->conditional('add_logo', '==', '1')
        ->endRepeater()
    ->endGroup();

return $testimonials;
?>
