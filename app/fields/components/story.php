<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$story = new FieldsBuilder('story');

$story
    ->addGroup('story', ['label' => 'Story'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTrueFalse('add_image')
            ->addImage('icon_bar', [
                'label' => 'Bar Icon',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_image', '==', '1')

        ->addImage('icon_mobile', [
            'label' => 'Icon Mobile',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])

        ->addRepeater('paragraphs', ['label' => 'Para'])
            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
        ->endRepeater()
    ->endGroup();


return $story;
?>
