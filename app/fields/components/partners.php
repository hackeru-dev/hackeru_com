<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$partners= new FieldsBuilder('partners');

$partners
    ->addGroup('partners', ['label' => 'Partners'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTrueFalse('add_image')
            ->addImage('image', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_image', '==', '1')

        ->addTrueFalse('add_image_mobile')
            ->addImage('image_mobile', [
                'label' => 'Image mobile',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_image_mobile', '==', '1')

        ->addRepeater('slides', ['label' => 'Slides'])
            ->addImage('slide_image', [
                'label' => 'Slide Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->endRepeater()
    ->endGroup();


return $partners;
?>
