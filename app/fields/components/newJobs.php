<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$newJobs= new FieldsBuilder('newJobs');

$newJobs
    ->addGroup('newJobs', ['label' => 'Hero'])
        ->addRepeater('headlines', ['label' => 'Headlines'])
        ->addText('headline', ['label' => 'Headline', 'required' => 1,'default_value' => 'Lorem Ipsum ',])
        ->addRadio('type_of_headline')
            ->addChoices(['normal' => 'Normal Headline'])
            ->addChoices(['marker' => 'Marker Headline'])
        ->endRepeater()

        ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
        ->addTrueFalse('add_button')
            ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])
        ->conditional('add_button', '==', '1')
    ->endGroup();

return $newJobs;
?>
