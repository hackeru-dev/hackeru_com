<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$numbers= new FieldsBuilder('numbers');

$numbers
    ->addGroup('numbers', ['label' => 'Numbers'])
        ->addRepeater('boxes', ['label' => 'Boxes'])
            ->addText('title', ['label' => 'Title','default_value' => '',])
            ->addText('title_special_char', ['label' => 'Title Special Character', 'required' => 0,'default_value' => '%',])
            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
        ->endRepeater()
    ->endGroup();

return $numbers;
?>
