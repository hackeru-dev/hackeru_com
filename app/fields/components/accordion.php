<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$accordion= new FieldsBuilder('accordion');

$accordion
    ->addGroup('accordion', ['label' => 'Accordion'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])

        ->addTrueFalse('add_paragraph')
            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
        ->conditional('add_paragraph', '==', '1')

        ->addTrueFalse('add_image_triangles')
            ->addImage('triangles_image', [
                'label' => 'Triangles Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->conditional('add_image_triangles', '==', '1')

        ->addImage('bar_image', [
            'label' => 'Bar Image',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])

        ->addImage('icon_up', [
            'label' => 'Icon Up',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
        ->addImage('icon_down', [
            'label' => 'Icon Down',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])

        ->addRepeater('boxes', ['label' => 'Slides Boxes'])
            ->addGroup('box_left', ['label' => 'Box Left'])
                ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
            ->endGroup()

            ->addGroup('box_right', ['label' => 'Box Right'])
                    ->addImage('image', [
                        'label' => 'Image',
                        'instructions' => '',
                        'required' => 0,
                        'return_format' => 'url',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                    ])
                    ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                    ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
                    ->addTrueFalse('add_button')
                        ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])
                    ->conditional('add_button', '==', '1')
                ->endGroup()
        ->endRepeater()
    ->endGroup();
return $accordion;
?>
