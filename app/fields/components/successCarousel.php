<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$successCarousel= new FieldsBuilder('successCarousel');

$successCarousel
    ->addGroup('successCarousel', ['label' => 'Success Carousel'])
        ->addTrueFalse('add_headline')
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->conditional('add_headline', '==', '1')

        ->addRepeater('boxes', ['label' => 'Boxes'])
            ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
            ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
            ->addImage('bg_image', [
                'label' => 'Background Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
        ->endRepeater()
    ->endGroup();
return $successCarousel;
?>
