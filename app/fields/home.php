<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$home= new FieldsBuilder('home');

$home
    ->setLocation('page_template', '==', 'views/page-home.blade.php');

$home
    ->addTab('# Hero #')
        ->addGroup('hero', ['label' => 'Hero'])
            ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addText('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
        ->endGroup();

$home
    ->addTab('# Carousel Programs  #')
        ->addFields(get_field_partial('components.carouselPrograms'));

$home
    ->addTab('# Academic  #')
        ->addGroup('academic', ['label' => 'Academic'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addTextarea('paragraph_2', ['label' => 'Paragraph 2', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addImage('icon_up', [
                'label' => 'Icon Up',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addImage('icon_down', [
                'label' => 'Icon Down',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addTrueFalse('add_mobile_image')
                ->addImage('mobile_image', [
                    'label' => 'Mobile Image',
                    'return_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ])
            ->conditional('add_mobile_image', '==', '1')

            ->addRepeater('boxes', ['label' => 'Box'])
                ->addGroup('box_left', ['label' => 'Box Left'])
                    ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                ->endGroup()

                ->addGroup('box_right', ['label' => 'Box Right'])
                    ->addImage('image', [
                        'label' => 'Image',
                        'instructions' => '',
                        'required' => 0,
                        'return_format' => 'url',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                    ])
                ->endGroup()
        ->endGroup();

$home
    ->addTab('# Partners  #')
        ->addFields(get_field_partial('components.partners'));

$home
    ->addTab('# Collage  #')
        ->addFields(get_field_partial('components.collage'));

$home
    ->addTab('# Success Carousel  #')
        ->addFields(get_field_partial('components.successCarousel'));

$home
    ->addTab('# Accordion #')
        ->addFields(get_field_partial('components.accordion'));

$home
    ->addTab('# Ecosystem Carousel #')
        ->addFields(get_field_partial('components.ecosystemCarousel'));

$home
    ->addTab('# Footer Form #')
        ->addFields(get_field_partial('components.footer_form'));

return $home;
?>
