<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$ourPrograms= new FieldsBuilder('ourPrograms');

$ourPrograms
    ->setLocation('page_template', '==', 'views/our-programs.blade.php');

$ourPrograms
    ->addTab('# Hero #')
    ->addGroup('hero', ['label' => 'Hero'])
        ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTextarea('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
    ->endGroup();

$ourPrograms
    ->addTab('# OurPrograms Image #')
    ->addGroup('ourPrograms_image', ['label' => 'ourPrograms Image'])
        ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addImage('triangles_img', [
            'label' => 'Icon Triangles',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
        ->addImage('bar_img', [
            'label' => 'Icon Bar',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
    ->endGroup();

$ourPrograms
    ->addTab('# Brain #')
        ->addGroup('brain', ['label' => 'Brain'])
            ->addRepeater('boxes', ['label' => 'Boxes'])
                ->addText('title', ['label' => 'Title', 'required' => 0,'default_value' => 'Lorem Ipsum',])
                ->addTextArea('paragraph', ['label' => 'Paragraph', 'default_value' => 'LoremLorem LoremLorem LoremLorem',])
            ->endRepeater()
        ->endGroup();

$ourPrograms
    ->addTab('# Carousel Programs  #')
        ->addFields(get_field_partial('components.carouselPrograms'));

$ourPrograms
    ->addTab('# Success Carousel  #')
        ->addFields(get_field_partial('components.successCarousel'));

$ourPrograms
    ->addTab('# Testimonials  #')
        ->addFields(get_field_partial('components.testimonials'));

$ourPrograms
    ->addTab('# Partners  #')
        ->addFields(get_field_partial('components.partners'));

$ourPrograms
    ->addTab('# Footer Form #')
        ->addFields(get_field_partial('components.footer_form'));

    return $ourPrograms;
?>
