<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$about= new FieldsBuilder('about');

$about
    ->setLocation('page_template', '==', 'views/about-us.blade.php');

$about
    ->addTab('# Hero #')
    ->addGroup('hero', ['label' => 'Hero'])
        ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
        ->addTextarea('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
        ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
        ->addRepeater('buttons', ['label' => 'Buttons'])
            ->addText('cta', ['label' => 'Button Text', 'default_value' => 'Lorem Ipsum',])
        ->endRepeater()
        ->addImage('icon_mobile', [
            'label' => 'Icon Mobile',
            'return_format' => 'url',
            'preview_size' => 'thumbnail',
            'library' => 'all',
        ])
    ->endGroup();

$about
    ->addTab('# Story  #')
        ->addFields(get_field_partial('components.story'));

$about
    ->addTab('# Accordion #')
        ->addFields(get_field_partial('components.accordion'));

$about
    ->addTab('# Success Carousel  #')
        ->addFields(get_field_partial('components.successCarousel'));

$about
    ->addTab('# Testimonials  #')
        ->addFields(get_field_partial('components.testimonials'));

$about
    ->addTab('# Partners  #')
        ->addFields(get_field_partial('components.partners'));

$about
    ->addTab('# Footer Form #')
        ->addFields(get_field_partial('components.footer_form'));

return $about;
?>
