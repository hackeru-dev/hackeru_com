<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$companies= new FieldsBuilder('companies');

$companies
    ->setLocation('page_template', '==', 'views/companies.blade.php');

$companies
    ->addTab('# Hero #')
        ->addGroup('hero', ['label' => 'Hero'])
            ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
        ->endGroup();

$companies
    ->addTab('# Services #')
        ->addGroup('services', ['label' => 'Services'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addImage('image', [
                'label' => 'Icon',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addRepeater('boxes', ['label' => 'Boxes'])
                ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
            ->endRepeater()
        ->endGroup();

$companies
    ->addTab('# Provider #')
        ->addGroup('provider', ['label' => 'Provider'])
            ->addText('headline', ['label' => 'Headline','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addText('marker', ['label' => 'Marker','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addTextarea('headline_left', ['label' => 'Headline Left','default_value' => 'Lorem Ipsum is simply dummy text of the',])
            ->addTextarea('headline_right', ['label' => 'Headline Right','default_value' => 'Lorem Ipsum is simply dummy text of the',])
            ->addImage('image_mobile', [
                'label' => 'Image Mobile',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])

            ->addRepeater('boxes_left', ['label' => 'Boxes Left'])
                ->addText('title_1', ['label' => 'Title 1', 'default_value' => 'Lorem',])
                ->addTextarea('title_2', ['label' => 'Title 2', 'default_value' => 'Lorem',])
            ->endRepeater()

            ->addRepeater('boxes_right', ['label' => 'Boxes Right'])
                ->addImage('icon', [
                    'label' => 'Icon',
                    'return_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ])
                ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
            ->endRepeater()
        ->endGroup();

$companies
    ->addTab('# Testing #')
        ->addGroup('testing', ['label' => 'Testing'])
            ->addText('headline_1', ['label' => 'Headline 1','default_value' => 'Lorem Ipsum is simply dummy text of the printing and',])
            ->addTextarea('paragraph_1', ['label' => 'Paragraph 1', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addText('headline_2', ['label' => 'Headline 2','default_value' => 'Lorem Ipsum is simply dummy text of the',])
            ->addTextarea('paragraph_2', ['label' => 'Paragraph 2', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->addImage('image_triangles', [
                'label' => 'Image',
                'return_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ])
            ->addRepeater('boxes', ['label' => 'Boxes'])
                ->addImage('icon', [
                    'label' => 'Icon',
                    'return_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                ])
                ->addText('title', ['label' => 'Title', 'default_value' => 'Lorem',])
                ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum is Lorem Ipsum is Lorem Ipsum is',])
            ->endRepeater()
        ->endGroup();

$companies
    ->addTab('# Ecosystem Carousel #')
        ->addFields(get_field_partial('components.ecosystemCarousel'));

$companies
    ->addTab('# Success Carousel  #')
        ->addFields(get_field_partial('components.successCarousel'));

$companies
    ->addTab('# Testimonials  #')
        ->addFields(get_field_partial('components.testimonials'));

$companies
    ->addTab('# Footer Form #')
        ->addFields(get_field_partial('components.footer_form'));

return $companies;
?>
