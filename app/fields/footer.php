<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$footer = new FieldsBuilder('page');

$footer
->setLocation('page_template', '==', 'views/partials/footer.blade.php');

$footer
->addGroup('footer', ['label' => 'Footer'])
    ->addText('headline_1', ['label' => 'Headline', 'default_value' => 'Lorem Ipsum ',])
    ->addTextarea('paragraph', ['label' => 'Paragraph', 'required' => 0,'default_value' => 'Lorem Ipsum',])
    ->addText('copyright_text', ['label' => 'Copyright Text', 'required' => 0,'default_value' => 'Lorem Ipsum',])
->endGroup();
return $footer;
