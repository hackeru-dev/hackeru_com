<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$page = new FieldsBuilder('page');

$page
    ->setLocation('post_type', '==', 'page');

$page
    ->addText('note', ['label' => 'Bottom Note', 'default_value' => 'Lorem Ipsum ',]);

return $page;
